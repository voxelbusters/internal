﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using VoxelBusters.CoreLibrary;
using VoxelBusters.CoreLibrary.Editor;

namespace {{Namespace}}.Editor
{
	[CustomEditor(typeof({{SettingsClassName}}))]
	public class {{SettingsInspectorClassName}} : SettingsObjectInspector
	{
        #region Base class methods

        protected override UnityPackageDefinition GetOwner()
        {
            return {{SettingsClassName}}.Package;
        }

		protected override InspectorDrawStyle GetDrawStyle()
        {
            return InspectorDrawStyle.Group;
        }

        protected override ButtonInfo[] GetTopBarButtons()
        {
            return new ButtonInfo[]
            {
                new ButtonInfo(label: "Tutorials",      onClick: {{EditorUtilityClassName}}.OpenTutorialsPage),
                new ButtonInfo(label: "Discord",        onClick: {{EditorUtilityClassName}}.OpenSupportPage),
                new ButtonInfo(label: "Write Review",	onClick: {{EditorUtilityClassName}}.OpenProductPage),
                new ButtonInfo(label: "Subscribe",		onClick: {{EditorUtilityClassName}}.OpenSubscribePage),
            };
        }

        protected override PropertyGroupInfo[] GetPropertyGroups()
        {
            return new PropertyGroupInfo[0];
        }

        protected override void DrawFooter()
        {
            base.DrawFooter();

            ShowMigrateToUpmOption();
        }

        #endregion
	}
}