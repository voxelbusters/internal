﻿using UnityEngine;
using System.Collections;

namespace {{Namespace}}
{
	internal static class AssetConstants
	{
		#region Paths

		public static string ExtrasPath => "{{PackagePath}}/Essentials";

        public static string EditorExtrasPath => $"{ExtrasPath}/Editor";

		public static string AndroidPluginPath => $"Assets/Plugins/Android";

		public static string AndroidProjectFolderName => "{{AndroidProjectName}}.androidlib";

		public static string AndroidProjectPath => $"{AndroidPluginPath}/{AndroidProjectFolderName}";

		#endregion
	}
}