﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor.DocFX
{
    public class DocumentationBuilderResult
    {
        #region Properties

        public string HtmlBuildPath { get; private set; }

        public string PdfBuildPath { get; private set; }

        #endregion

        #region Constructors

        public DocumentationBuilderResult(string htmlBuildPath, string pdfBuildPath)
        {
            // set properties
            HtmlBuildPath   = htmlBuildPath;
            PdfBuildPath    = pdfBuildPath;
        }

        #endregion
    }
}