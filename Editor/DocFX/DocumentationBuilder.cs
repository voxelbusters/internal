﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor.DocFX
{
    public class DocumentationBuilder
    {
        #region Constants

        private     const       string      kBuildEndMessage      = "Finished...";

        #endregion

        #region Static methods

        public static void Build(string configFilePath, bool clearCache = true, System.Action<DocumentationBuilderResult, Error> completionCallback = null)
        {
            string  configFileName  = Path.GetFileName(configFilePath);
            string  parentFolder    = Path.GetDirectoryName(configFilePath);

            // remove cached data
            string  htmlBuildPath   = $"{parentFolder}/_site";
            string  pdfBuildPath    = $"{parentFolder}/_site";
            if (clearCache)
            {
                if (Directory.Exists($"{parentFolder}/obj"))
                {
                    Directory.Delete($"{parentFolder}/obj", recursive: true);
                }
                if (Directory.Exists(htmlBuildPath))
                {
                    Directory.Delete(htmlBuildPath, recursive: true);
                }
                if (Directory.Exists(pdfBuildPath))
                {
                    Directory.Delete(pdfBuildPath, recursive: true);
                }
            }

            // create build script
            var     scriptFileName      = "BuildDocFX.sh";
            CreateBuildScript(scriptPath: $"{parentFolder}/{scriptFileName}", configFileName: configFileName);

            // run export script
            var     environmentPaths    = new string[] { "/Library/Frameworks/Mono.framework/Versions/Current/bin", "/usr/local/bin", "~/.dotnet/tools" };
            var     process             = ShellHelper.ProcessCommand("bash " + scriptFileName, parentFolder, new List<string>(environmentPaths));
            bool    isDone              = false;
            process.onDone      += () =>
            {
                var     result          = new DocumentationBuilderResult(
                    htmlBuildPath: Directory.Exists(htmlBuildPath) ? htmlBuildPath : null,
                    pdfBuildPath: Directory.Exists(pdfBuildPath) ? pdfBuildPath : null);
                OnBuildComplete(result: result, error: null, buildStatus: ref isDone, completionCallback: completionCallback);
            };
            process.onError     += () =>
            {
                OnBuildComplete(result: null, error: new Error("Unknown error"), buildStatus: ref isDone, completionCallback: completionCallback);
            };
            process.onLog       += (type, message) =>
            {
                // hack to identify end of build process
                if (type == 0 && string.Equals(kBuildEndMessage, message))
                {
                    var     result          = new DocumentationBuilderResult(
                        htmlBuildPath: Directory.Exists(htmlBuildPath) ? htmlBuildPath : null,
                        pdfBuildPath: Directory.Exists(pdfBuildPath) ? pdfBuildPath : null);
                    OnBuildComplete(result: result, error: null, buildStatus: ref isDone, completionCallback: completionCallback);
                }
            };
        }

        private static void OnBuildComplete(DocumentationBuilderResult result, Error error, ref bool buildStatus, System.Action<DocumentationBuilderResult, Error> completionCallback)
        {
            // check whether build operation is already completed
            if (buildStatus)
            {
                return;
            }

            // mark that action is completed
            buildStatus  = true;

            // send event
            completionCallback?.Invoke(result, error);
        }

        private static void CreateBuildScript(string scriptPath, string configFileName)
        {
            var     builder = new StringBuilder();
            builder.AppendLine("#!/bin/bash");
            builder.AppendLine();
            builder.AppendLine("echo \"Processing...\"");
            builder.AppendLine();
            builder.AppendLine(string.Format("FILENAME=\"{0}\"", configFileName));
            builder.AppendLine($"docfx ${{FILENAME}}");
            builder.AppendLine();
            builder.AppendLine("echo \"Finished...\"");

            File.WriteAllText(scriptPath, builder.ToString());
        }

        #endregion
    }
}