﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Compilation;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    public class AssemblyBuilderResult
    {
        #region Private fields

        public AssembliesType AssembliesType { get; private set; }

        public string[] Assemblies { get; private set; }

        #endregion

        #region Constructors

        public AssemblyBuilderResult(AssembliesType assembliesType, string[] assemblies = null)
        {
            // set properties
            AssembliesType  = assembliesType;
            Assemblies      = assemblies ?? new string[0];
        }

        #endregion
    }
}