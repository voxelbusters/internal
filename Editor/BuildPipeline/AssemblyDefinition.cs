﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor
{
    [Serializable]
    public class AssemblyDefinition
    {
        #region Fields

        [SerializeField]
        private     string                      m_name;
        
        [SerializeField]
        private     string                      m_guid;

        [SerializeField]
        private     string                      m_namespace;

        [SerializeField]
        private     AssemblyType                m_type;

        [SerializeField, SystemLibraryName]
        private     string[]                    m_systemLibraryReferences;

        [SerializeField, ExternalLibraryName]
        private     string[]                    m_externalLibraryReferences;

        [SerializeField, AssemblyName]
        private     string[]                    m_additionalReferences;

        [SerializeField]
        private     string                      m_additionalDefines;

        [SerializeField]
        private     SourceFolderDefinition[]    m_sourcefolders;

        [SerializeField, FileBrowser(usesRelativePath: true)]
        private     string[]                    m_sourceFiles;

        [SerializeField]
        private     bool                        m_inheritFromAsmDef;

        #endregion

        #region Properties

        public string Name => m_name;

        public string Guid { get => m_guid; set => m_guid = value; }

        public string Namespace => m_namespace;

        public AssemblyType Type => m_type;

        public string[] SystemLibraryReferences => m_systemLibraryReferences;

        public string[] ExternalLibraryReferences => m_externalLibraryReferences;

        public string[] AdditionalReferences => m_additionalReferences;

        public string[] AdditionalDefines => m_additionalDefines.Split(';');

        public SourceFolderDefinition[] SourceFolders => m_sourcefolders;

        public string[] SourceFiles => Array.ConvertAll(m_sourceFiles, (item) => IOServices.GetAbsolutePath(item));

        public bool InheritFromAsmDef => m_inheritFromAsmDef;

        #endregion

        #region Constructors

        public AssemblyDefinition(string name, string guid,
            string ns, AssemblyType type,
            string[] systemLibraryReferences, string[] externalLibraryReferences,
            string[] additionalReferences, string[] additionalDefines,
            SourceFolderDefinition[] sourcefolders, string[] sourcefiles)
        {
            // set properties
            m_name                      = name;
            m_guid                      = guid;
            m_namespace                 = ns;
            m_type                      = type;
            m_systemLibraryReferences   = systemLibraryReferences;
            m_externalLibraryReferences = externalLibraryReferences;
            m_additionalReferences      = additionalReferences;
            m_additionalDefines         = (additionalDefines != null) ? string.Join(";", additionalDefines) : string.Empty;
            m_sourcefolders             = sourcefolders;
            m_sourceFiles               = sourcefiles;
        }

        #endregion

        #region Nested types

        public enum AssemblyType
        {
            Player,

            Editor,

            Test
        }

        #endregion
    }
}