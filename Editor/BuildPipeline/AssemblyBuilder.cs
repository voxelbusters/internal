﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor
{
    public class AssemblyBuilder
    {
        #region Fields

        private     string                      m_guid;

        private     List<AssemblyDefinition>    m_assemblies;

        private     List<LibraryDefinition>     m_externalLibraries;

        private     ScriptCompilerOptions       m_compilerOptions;

        private     string                      m_outputPath;

        #endregion

        #region Properties

        public AssemblyBuilderStatus Status { get; set; }

        #endregion

        #region Static events

        public static EventCallback<AssemblyBuilderResult> OnCompilationComplete;

        #endregion

        #region Constructors

        public AssemblyBuilder(string guid, List<AssemblyDefinition> assemblies,
            List<LibraryDefinition> externalLibraries, ScriptCompilerOptions compilerOptions,
            string outputPath)
        {
            // set properties
            m_guid              = guid;
            m_assemblies        = assemblies;
            m_externalLibraries = externalLibraries;
            m_compilerOptions   = compilerOptions;
            m_outputPath        = outputPath;
            Status              = AssemblyBuilderStatus.NotStarted;
        }

        #endregion

        #region Static methods

        private static string[] GetUnityDefines(BuildTarget buildTarget, AssembliesType assembliesType, bool usePlayerDefines)
        {
            var     defines     = new List<string>();

            // editor specifc defines
            if (assembliesType == AssembliesType.Editor)
            {
                defines.Add("UNITY_EDITOR");
            }
            var     unityVersion        = Application.unityVersion;
            var     versionComponents   = unityVersion.Split('.');
            var     unityMajorVersion   = int.Parse(versionComponents[0]);
            var     unityMinorVersion   = int.Parse(versionComponents[1]);
            var     unityPatchNumber    = Regex.Split(versionComponents[2], @"\D+")[0];
            for (int majorVersion = 2017; majorVersion <= unityMajorVersion; majorVersion++)
            {
                int     maxMinorVersion = (majorVersion == unityMajorVersion) ? unityMinorVersion : 4;
                for (int minorVersion = 1; minorVersion <= maxMinorVersion; minorVersion++)
                {
                    defines.Add($"UNITY_{majorVersion}_{minorVersion}_OR_NEWER");
                }
            }
            defines.Add($"UNITY_{unityMajorVersion}");
            defines.Add($"UNITY_{unityMajorVersion}_{unityMinorVersion}");
            defines.Add($"UNITY_{unityMajorVersion}_{unityMinorVersion}_{unityPatchNumber}");

            // target specific defines
            switch (buildTarget)
            {
                case BuildTarget.Android:
                    defines.Add("UNITY_ANDROID");
                    break;

                case BuildTarget.iOS:
                    defines.Add("UNITY_IOS");
                    break;

                case BuildTarget.StandaloneOSX:
                    defines.Add("UNITY_STANDALONE_OSX");
                    break;

                case BuildTarget.WebGL:
                    defines.Add("UNITY_WEBGL");
                    break;
            }

            // player scripting symbols
            if (usePlayerDefines)
            {
                var     userDefines    = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildPipeline.GetBuildTargetGroup(buildTarget)).Split(';');
                defines.AddRange(userDefines);
            }

            return defines.ToArray();
        }

        #endregion

        #region Getter methods

        private LibraryDefinition GetExternalLibrary(string name)
        {
            return m_externalLibraries.Find((item) => string.Equals(item.Name, name));
        }

        private string GetUnityProjectName()
        {
            var     pathComponents  = Application.dataPath.Split('/');
            return pathComponents[pathComponents.Length - 2];
        }

        private string GetUnityProjectVersion()
        {
            return Application.version;
        }

        #endregion

        #region Public methods

        public void Build(CompletionCallback completionCallback = null)
        {
            Build(EditorUserBuildSettings.activeBuildTarget, completionCallback);
        }

        public void Build(BuildTarget buildTarget, CompletionCallback completionCallback = null)
        {
            BuildEditorAssemblies(buildTarget, (editorBuildResult, editorBuildError) =>
            {
                if (editorBuildError == null)
                {
                    BuildPlayerAssemblies(buildTarget, (playerBuildResult, playerBuildError) =>
                    {
                        if (playerBuildError == null)
                        {
                            completionCallback?.Invoke(success: true, error: null);
                        }
                        else
                        {
                            completionCallback?.Invoke(success: false, error: playerBuildError);
                        }
                    });
                }
                else
                {
                    completionCallback?.Invoke(success: false, error: editorBuildError);
                }
            });
        }

        public void BuildPlayerAssemblies(BuildTarget buildTarget, EventCallback<AssemblyBuilderResult> completionCallback = null)
        {
            BuildAssemblies(buildTarget, AssembliesType.Player, completionCallback);
        }

        public void BuildEditorAssemblies(BuildTarget buildTarget, EventCallback<AssemblyBuilderResult> completionCallback = null)
        {
            BuildAssemblies(buildTarget, AssembliesType.Editor, completionCallback);
        }

        #endregion

        #region Private methods

        private void BuildAssemblies(BuildTarget buildTarget, AssembliesType assembliesType, EventCallback<AssemblyBuilderResult> completionCallback = null)
        {
            // update state value
            Status  = AssemblyBuilderStatus.Compiling;

            // create folders specific to assembly types
            string  parentFolder        = m_outputPath.TrimEnd('/');
            string  solutionFolder      = IOServices.GetAbsolutePath($"{parentFolder}/Temp{assembliesType}");
            string  outputFolder        = IOServices.GetAbsolutePath($"{parentFolder}/{assembliesType}ScriptAssemblies");
            IOServices.CreateDirectory(solutionFolder, overwrite: true);
            IOServices.CreateDirectory(outputFolder, overwrite: true);

            // find all unity compiled libraries 
            var     assemblies          = GetAssemblies(buildTarget, assembliesType);

            // configure solution
            string  solutionFileName    = $"{GetUnityProjectName()}.sln";
            string  solutionFilePath    = Path.Combine(solutionFolder, solutionFileName);
            var     solution            = new SlnFileWriter(
                name: GetUnityProjectName(),
                version: GetUnityProjectVersion(),
                guid: m_guid,
                path: solutionFilePath);
                       
            // add project files
            foreach (var currentAssembly in assemblies)
            {
                string  projectFilePath = Path.Combine(solutionFolder, $"{currentAssembly.Name}.csproj");
                var     project         = new CsprojFileWriter(
                    name: currentAssembly.Name,
                    ns: currentAssembly.Namespace,
                    version: GetUnityProjectVersion(),
                    guid: currentAssembly.Guid,
                    path: projectFilePath);
                project.CompilerOptions = m_compilerOptions;

                foreach (var reference in currentAssembly.LibraryReferences)
                {
                    project.AddLibraryReference(reference.Path);
                }

                foreach (var reference in currentAssembly.AdditionalReferences)
                {
                    var     csprojRef   = solution.GetProjectWithGuid(reference.Guid);
                    project.AddProjectReference(csprojRef);
                }

                foreach (var item in currentAssembly.Defines)
                {
                    project.AddDefineConstant(item);
                }

                foreach (var item in currentAssembly.SourceFiles)
                {
                    project.AddSourceFile(item);
                }

                // save project file
                var     projRef = project.Save();

                // add project to solution
                solution.AddProject(projRef);
            }
                
            // save solution file
            solution.Save();

            // create shell script with export instructions
            string  shellScriptName     = "BuildScriptAssemblies.sh";
            string  shellScriptFilePath = Path.Combine(solutionFolder, shellScriptName);
            CreateBuildScript(shellScriptFilePath, m_compilerOptions.BuildConfiguration);

            // run export script
            var     environmentPaths    = new string[] { "/Library/Frameworks/Mono.framework/Versions/Current/bin" };
            var     process             = ShellHelper.ProcessCommand("bash " + shellScriptName, solutionFolder, new List<string>(environmentPaths));
            process.onDone      += () =>
            {
                string  binFolderName   = SlnBuildUtility.GetOutputFolderForConfiguration(m_compilerOptions.BuildConfiguration);
                string  binFolder       = Path.Combine(solutionFolder, binFolderName);
                var     result          = PostprocessBuild(assemblies, assembliesType, sourceFolder: binFolder, outputFolder: outputFolder, out Error error);

                // update state value
                Status  = AssemblyBuilderStatus.Finished;

                // send event
                completionCallback?.Invoke(result, error);
                OnCompilationComplete?.Invoke(result, error);
            };
            process.onError     += () =>
            {
                completionCallback?.Invoke(null, new Error("Unknown error"));
            };
        }

        private Assembly[] GetAssemblies(BuildTarget buildTarget, AssembliesType assembliesType)
        {
            var     buildTargetGroup            = BuildPipeline.GetBuildTargetGroup(buildTarget);
            var     unityDefinedDefines         = GetUnityDefines(buildTarget, assembliesType, m_compilerOptions.UsePlayerDefines);
            var     unityCompiledAssemblies     = CompilationPipeline.GetAssemblies(assembliesType);

            var     sortedAssemblies            = new SortedAssemblyCollection(m_assemblies.ToArray()).Sort();
            var     targetAssemblies            = new List<Assembly>();
            foreach (var asmDef in sortedAssemblies)
            {
                if ((assembliesType == AssembliesType.Player) && (asmDef.Type != AssemblyDefinition.AssemblyType.Player))
                {
                    continue;
                }

                // gather required properties
                var     libraryReferences       = new List<LibraryDefinition>();
                libraryReferences.AddRange(SystemLibraryHelpers.GetDefaultPlayerLibraries());
                if (assembliesType == AssembliesType.Editor)
                {
                    libraryReferences.AddRange(SystemLibraryHelpers.GetDefaultEditorLibraries());
                }
                libraryReferences.AddRange(Array.ConvertAll(asmDef.SystemLibraryReferences, (item) => SystemLibraryHelpers.GetSystemLibrary(item)));
                libraryReferences.AddRange(Array.ConvertAll(asmDef.ExternalLibraryReferences, (item) => GetExternalLibrary(item)));

                var     additionalReferences    = Array.ConvertAll(asmDef.AdditionalReferences, (refererence) =>
                {
                    return targetAssemblies.Find((item) => string.Equals(refererence, item.Name));
                });

                var     defines                 = new List<string>(unityDefinedDefines);
                defines.AddRange(asmDef.AdditionalDefines);

                var     sourceFiles             = asmDef.InheritFromAsmDef
                    ? Array.Find(unityCompiledAssemblies, (item) => string.Equals(item.name, asmDef.Name)).sourceFiles
                    : asmDef.SourceFiles;

                // create assembly object
                var     assemblyObj             = new Assembly(
                    name: asmDef.Name,
                    guid: asmDef.Guid,
                    ns: asmDef.Namespace,
                    libraryReferences: libraryReferences.ToArray(),
                    additionalReferences: additionalReferences,
                    defines: defines.ToArray(),
                    sourceFiles: sourceFiles);
                targetAssemblies.Add(assemblyObj);  
            }
            return targetAssemblies.ToArray();
        }

        #endregion

        #region Post processing methods

        private void CreateBuildScript(string path, BuildConfiguration configuration)
        {
            var     builder = new StringBuilder();
            builder.AppendLine("#!/bin/bash");
            builder.AppendLine();
            builder.AppendLine("echo \"Processing...\"");
            builder.AppendLine();
            builder.AppendLine("#compile all the projects");
            builder.AppendLine(string.Format("FILENAME=\"{0}\"", GetUnityProjectName() + ".sln"));
            builder.AppendLine(string.Format("msbuild ${{FILENAME}} -p:Configuration={0}", SlnBuildUtility.ConvertBuildConfigurationToString(configuration)));
            builder.AppendLine();
            builder.Append("echo \"Done!\"");

            File.WriteAllText(path, builder.ToString());
        }

        private AssemblyBuilderResult PostprocessBuild(Assembly[] assemblies, AssembliesType assembliesType, string sourceFolder, string outputFolder, out Error error)
        {
            // set reference values
            error   = null;

            // check build status
            int     assemblyCount       = assemblies.Length;
            var     binaryFiles         = new List<string>(assemblyCount);
            for (int iter = 0; iter < assemblyCount; iter++)
            {
                var     assembly        = assemblies[iter];
                string  sourceFileName  = $"{assembly.Name}.dll";
                string  sourceFilePath  = Path.Combine(sourceFolder, sourceFileName);
                if (!File.Exists(sourceFilePath))
                {
                    Debug.LogWarning("Could not find binary files for project: " + sourceFileName);
                    continue;
                }

                binaryFiles.Add(sourceFilePath);
            }

            if (binaryFiles.Count != assemblyCount)
            {
                IOServices.ClearDirectory(outputFolder);

                error   = new Error("Build failed");
                return new AssemblyBuilderResult(assembliesType: assembliesType);
            }

            // copy files
            foreach (var file in binaryFiles)
            {
                var     targetFilePath      = Path.Combine(outputFolder, Path.GetFileName(file));
                File.Copy(file, targetFilePath);
            }

            return new AssemblyBuilderResult(assembliesType: assembliesType, assemblies: Array.ConvertAll(assemblies, (item) => item.Name));
        }

        #endregion

        #region Nested types

        private class SortedAssemblyCollection
        {
            #region Fields

            private     AssemblyDefinition[]                        m_assemblies;

            private     Dictionary<string, AssemblyDefinition>      m_assemblyMap;

            private     Dictionary<string, bool>                    m_visitedAssemblies;

            private     List<AssemblyDefinition>                    m_sortedAssemblies;

            #endregion

            #region Constructors

            public SortedAssemblyCollection(AssemblyDefinition[] assemblies)
            {
                // set properties
                m_assemblies        = assemblies;
                m_assemblyMap       = new Dictionary<string, AssemblyDefinition>();
                m_visitedAssemblies = new Dictionary<string, bool>();
                m_sortedAssemblies  = new List<AssemblyDefinition>();

                // add map entries
                foreach (var asmDef in m_assemblies)
                {
                    m_assemblyMap.Add(asmDef.Name, asmDef);
                }
            }

            #endregion

            #region Public methods

            public AssemblyDefinition[] Sort()
            {
                // start iterating through objects
                foreach (var item in m_assemblies)
                {
                    if (!IsVisited(item.Name))
                    {
                        DependencySortHandler(item);
                    }
                }

                return m_sortedAssemblies.ToArray();
            }

            private void DependencySortHandler(AssemblyDefinition assembly)
            {
                m_visitedAssemblies[assembly.Name]  = true;

                foreach (var reference in assembly.AdditionalReferences)
                {
                    if (!IsVisited(reference))
                    {
                        DependencySortHandler(m_assemblyMap[reference]);
                    }
                }
                m_sortedAssemblies.Add(assembly);
            }

            private bool IsVisited(string assembly)
            {
                return m_visitedAssemblies.TryGetValue(assembly, out bool visited) && visited;
            }

            #endregion
        }

        #endregion
    }

    /*
    internal class AssemblyBuilderInternal
    {
        #region Fields

        private string[] m_defines;

        private string[] m_additionalDefines;

        private string[] m_defaultReferences;

        private string[] m_additionalReferences;

        private ScriptCompilerOptions m_compilerOptions;

        #endregion

        #region Fields

        public string[] ScriptPaths { get; private set; }

        public string AssemblyPath { get; private set; }

        public BuildTarget BuildTarget { get; set; }

        public string[] Defines { get => m_defines ?? new string[0]; set => m_defines = value; }

        public string[] AdditionalDefines { get => m_additionalDefines ?? new string[0]; set => m_additionalDefines = value; }

        public string[] DefaultReferences { get => m_defaultReferences ?? new string[0]; set => m_defaultReferences = value; }

        public string[] AdditionalReferences { get => m_additionalReferences ?? new string[0]; set => m_additionalReferences = value; }

        public ScriptCompilerOptions CompilerOptions { get => m_compilerOptions ?? new ScriptCompilerOptions(); set => m_compilerOptions = value; }

        #endregion

        #region Constructors

        public AssemblyBuilderInternal(string[] scriptPaths, string assemblyPath, BuildTarget buildTarget)
        {
            // set properties
            ScriptPaths     = scriptPaths;
            AssemblyPath    = assemblyPath;
            BuildTarget     = buildTarget;
        }

        #endregion

        #region Public methods

        public bool Build()
        {
            if (Directory.Exists(kTempAssembliesDirectory))
                Directory.Delete(kTempAssembliesDirectory, recursive: true);
            Directory.CreateDirectory(kTempAssembliesDirectory);

            try
            {
                var providerOptions = new Dictionary<string, string>();
                providerOptions.Add("CompilerVersion", CompilerOptions.FrameworkVersion);

                var codeProvider = new CSharpCodeProvider(providerOptions);
                var rspFilePath = GenerateCompilerOptionsFile();
                var compilerParams = new CompilerParameters
                {
                    GenerateExecutable = false,
                    OutputAssembly = AssemblyPath,
                    TreatWarningsAsErrors = false,
                    IncludeDebugInformation = true,
                    GenerateInMemory = true,
                    CompilerOptions = string.Format("@\"{0}\"", rspFilePath),
                    WarningLevel = 0,
                };

                // compile the DLL then dispose of code provider
                var scriptFullPaths = Array.ConvertAll(ScriptPaths, (item) => Path.GetFullPath(item));
                var compilerResults = codeProvider.CompileAssemblyFromFile(compilerParams, scriptFullPaths);
                codeProvider.Dispose();

                if (compilerResults.Errors.Count > 0)
                {
                    Debug.LogErrorFormat("Errors building {0} into {1}", Path.GetFileName(AssemblyPath), AssemblyPath);
                    foreach(var error in compilerResults.Errors)
                    {
                        Debug.LogErrorFormat("{0}", error.ToString());
                    }
                    return false;
                }
                else
                {
                    Debug.LogFormat("Source {0} built into {1} successfully.", Path.GetFileName(AssemblyPath), AssemblyPath);
                    return true;
                }
            }
            finally
            {
                //IOServices.DeleteDirectory(kTempAssembliesDirectory);
            }
        }

        #endregion

        #region Private methods

        private const string kTempResponseFileName = "temp_response_file.rsp";
        private static readonly string kTempAssembliesDirectory 
            = Path.Combine(Path.GetDirectoryName(Application.dataPath), "TempAssemblies");
        private string GenerateCompilerOptionsFile()
        {
            var responseFileString = new StringBuilder();
            var compileSymbols = string.Join(";", MergeDefineSymbols(Defines, AdditionalDefines));

            var rspFilePath = Path.Combine(kTempAssembliesDirectory, kTempResponseFileName);

            responseFileString.AppendLine(string.Format("-out:\"{0}\"", AssemblyPath));
            responseFileString.AppendLine("-target:library");

            if (compileSymbols.Length > 0)
                responseFileString.AppendLine(string.Format("-define:\"{0}\"", compileSymbols));

            if (CompilerOptions.Optimize)
                responseFileString.AppendLine("-optimize");

            if (CompilerOptions.Unsafe)
                responseFileString.AppendLine("-unsafe");

            // append additional arguments
            foreach (var additionalArgument in CompilerOptions.AdditionalArguments)
            {
                responseFileString.AppendLine(additionalArgument);
            }
            responseFileString.AppendLine();


        // append base references
        responseFileString.AppendLine("-reference:\"System.dll\"");
        responseFileString.AppendLine("-reference:\"System.Core.dll\"");
        responseFileString.AppendLine("-reference:\"System.Xml.dll\"");

            var referencesMap = new Dictionary<string, string>();
            
            // process default library paths
            foreach (var path in DefaultReferences)
            {
                var libName = Path.GetFileName(path) ?? string.Empty;
                if (string.IsNullOrEmpty(libName) || referencesMap.ContainsKey(libName))
                    continue;

                referencesMap.Add(libName, path);
            }

            // process additional references
            foreach (var path in AdditionalReferences)
            {
                var libName = Path.GetFileName(path) ?? string.Empty;
                if (string.IsNullOrEmpty(libName) || referencesMap.ContainsKey(libName))
                    continue;

                referencesMap.Add(libName, path);
            }

            // add all references to .rsp file
            foreach (var reference in referencesMap.Values)
            {
                responseFileString.AppendLine(string.Format("-reference:\"{0}\"", reference));
            }

            #if NET_STANDARD_2_0
            var netstandard = Assembly.Load("netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51");
            responseFileString.AppendLine(string.Format("-reference:\"{0}\"", netstandard.Location));
            #endif

            responseFileString.AppendLine();

            // append all script file paths
            foreach (var scriptPath in ScriptPaths)
            {
                var     scriptAbsolutePath  = Path.GetFullPath(scriptPath);
                //responseFileString.AppendLine(string.Format("\"{0}\"", scriptAbsolutePath));
            }

            File.WriteAllText(rspFilePath, responseFileString.ToString(), Encoding.UTF8);
            return rspFilePath;
        }

        private string[] MergeDefineSymbols(string[] defaultDefines, string[] additionalDefines)
        {
            var     combinedSymbols = new List<string>(defaultDefines);
            foreach (var item in additionalDefines)
            {
                if (!combinedSymbols.Contains(item))
                    combinedSymbols.Add(item);
            }
            return combinedSymbols.ToArray();
        }

        #endregion
    }
    */
}