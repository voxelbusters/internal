﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor
{
    [System.Serializable]
    public class LibraryDefinition
    {
        #region Fields

        [SerializeField]
        private     string                  m_name;

        [SerializeField, FileBrowser(usesRelativePath: true)]
        private     string                  m_path;

        #endregion

        #region Properties

        public string Name => m_name;

        public string Path => IOServices.GetAbsolutePath(m_path);

        #endregion

        #region Fields

        public LibraryDefinition(string name, string path)
        {
            // set properties
            m_name      = name;
            m_path      = path;
        }

        #endregion
    }
}