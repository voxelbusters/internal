﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor
{
    public class SlnFileWriter
    {
        #region Constants

        private     const   int     kTabSize        = 4;

        #endregion

        #region Fields

        private     List<CsprojReference>           m_projects;

        private     int                             m_indentationLevel;
        
        #endregion

        #region Properties

        public string Name { get; private set; }

        public string Version { get; private set; }

        public string Guid { get; private set; }

        public string Path { get; private set; }

        #endregion

        #region Constructors

        public SlnFileWriter(string name, string version, string guid, string path)
        {
            // set properties
            Name            = name;
            Version         = version;
            Guid            = guid;
            Path            = path;

            m_projects      = new List<CsprojReference>();
        }

        #endregion

        #region Public methods

        public void AddProject(CsprojReference project)
        {
            m_projects.Add(project);
        }

        public CsprojReference GetProjectWithGuid(string guid)
        {
            return m_projects.Find((item) => string.Equals(item.Guid, guid));
        }

        #endregion

        #region Getter methods

        public CsprojReference FindProjectWithName(string name)
        {
            return m_projects.Find((obj) => string.Equals(name, obj.Name));
        }

        #endregion

        #region Serialize methods

        public void Save()
        {
            // get information
            string  absolutePath    = IOServices.GetAbsolutePath(Path);
            string  parentFolder    = System.IO.Path.GetDirectoryName(absolutePath);

            // prepare
            m_indentationLevel      = 0;
            Directory.CreateDirectory(parentFolder);

            // write to file
            var     availableConfigurations = Enum.GetValues(typeof(BuildConfiguration));
            using (var streamWriter = new StreamWriter(absolutePath))
            {
                // add header
                WriteLine(streamWriter, "");
                WriteLine(streamWriter, "Microsoft Visual Studio Solution File, Format Version 12.00");
                WriteLine(streamWriter, "# Visual Studio 15");

                // add project references
                foreach (var reference in m_projects)
                {
                    string projectRelativePath  = IOServices.GetRelativePath(parentFolder, reference.Path);
                    WriteLine(streamWriter, string.Format("Project(\"{{{0}}}\") = \"{1}\", \"{2}\", \"{{{3}}}\"", Guid, reference.Name, projectRelativePath, reference.Guid));
                    WriteLine(streamWriter, "EndProject");
                }

                // add global section
                WriteLine(streamWriter, "Global");
                m_indentationLevel++;

                // add configuration section
                WriteLine(streamWriter, "GlobalSection(SolutionConfigurationPlatforms) = preSolution");
                m_indentationLevel++;
                foreach (BuildConfiguration configuration in availableConfigurations)
                {
                    WriteLine(streamWriter, string.Format("{0}|Any CPU = {0}|Any CPU", SlnBuildUtility.ConvertBuildConfigurationToString(configuration)));
                }
                m_indentationLevel--;
                WriteLine(streamWriter, "EndGlobalSection");

                // project configuration section
                WriteLine(streamWriter, "GlobalSection(ProjectConfigurationPlatforms) = postSolution");
                m_indentationLevel++;
                foreach (var project in m_projects)
                {
                    foreach (BuildConfiguration configuration in availableConfigurations)
                    {
                        WriteLine(streamWriter, string.Format("{{{0}}}.{1}|Any CPU.ActiveCfg = {1}|Any CPU", project.Guid, SlnBuildUtility.ConvertBuildConfigurationToString(configuration)));
                        WriteLine(streamWriter, string.Format("{{{0}}}.{1}|Any CPU.Build.0 = {1}|Any CPU", project.Guid, SlnBuildUtility.ConvertBuildConfigurationToString(configuration)));
                    }
                }
                m_indentationLevel--;
                WriteLine(streamWriter, "EndGlobalSection");

                // monodevelop properties section
                //WriteLine(streamWriter, "GlobalSection(MonoDevelopProperties) = preSolution");
                //m_indentationLevel++;
                //WriteLine(streamWriter, "Policies = $0");
                //WriteLine(streamWriter, "$0.DotNetNamingPolicy = $1");
                //WriteLine(streamWriter, "$1.DirectoryNamespaceAssociation = PrefixedHierarchical");
                //WriteLine(streamWriter, "$0.TextStylePolicy = $2");
                //WriteLine(streamWriter, "$2.FileWidth = 80");
                //WriteLine(streamWriter, "$2.TabsToSpaces = True");
                //WriteLine(streamWriter, "$2.scope = text/x-csharp");
                //WriteLine(streamWriter, "$0.CSharpFormattingPolicy = $3");
                //WriteLine(streamWriter, "$3.scope = text/x-csharp");
                //WriteLine(streamWriter, "$0.TextStylePolicy = $4");
                //WriteLine(streamWriter, "$4.FileWidth = 80");
                //WriteLine(streamWriter, "$4.TabsToSpaces = True");
                //WriteLine(streamWriter, "$4.scope = text/plain");
                //WriteLine(streamWriter, "version = " + Version);
                //m_indentationLevel--;
                //WriteLine(streamWriter, "EndGlobalSection");

                m_indentationLevel--;
                WriteLine(streamWriter, "EndGlobal");

                // commit information
                streamWriter.Close();
            }
        }

        #endregion

        #region Formatter methods

        private void WriteLine(StreamWriter writer, string value)
        {
            // append indentation
            for (int level = 0; level < m_indentationLevel; level++)
            {
                writer.Write("\t");
            }

            // append text
            writer.WriteLine(value);
        }

        #endregion
    }
}