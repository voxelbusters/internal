﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    public static class XmlUtility
    {
        public static void WriteElement(this XmlWriter xmlWriter, string element, string attributeName, string attributeValue, string value = null)
        {
            xmlWriter.WriteStartElement(element);
            xmlWriter.WriteAttributeString(attributeName, attributeValue);
            if (value != null)
            {
                xmlWriter.WriteString(value);
            }
            xmlWriter.WriteEndElement();
        }

        public static void WriteElement(this XmlWriter xmlWriter, string element, string value)
        {
            xmlWriter.WriteStartElement(element);
            xmlWriter.WriteString(value);
            xmlWriter.WriteEndElement();
        }

        public static void WriteElement(this XmlWriter xmlWriter, string element, bool value)
        {
            xmlWriter.WriteStartElement(element);
            xmlWriter.WriteValue(value);
            xmlWriter.WriteEndElement();
        }

        public static void WriteElement(this XmlWriter xmlWriter, string element, int value)
        {
            xmlWriter.WriteStartElement(element);
            xmlWriter.WriteValue(value);
            xmlWriter.WriteEndElement();
        }
    }
}