﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    [Serializable]
    public class CsprojBuildCommand
    {
        #region Fields
        
        [SerializeField]
        private     SlnBuildEventType       m_eventType;

        [SerializeField]
        private     BuildConfiguration      m_configuration;

        [SerializeField]
        private     string                  m_command;

        #endregion

        #region Properties

        public SlnBuildEventType EventType => m_eventType; 

        public BuildConfiguration Configuration => m_configuration;

        public string Command => m_command;

        #endregion
    }

    public enum SlnBuildEventType
    {
        PostBuild
    }
}