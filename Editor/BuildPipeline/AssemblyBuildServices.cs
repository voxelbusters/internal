﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Player;
using UnityEditor.Compilation;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor
{
    public class AssemblyBuildServices
    {
        #region Static methods

        public static void ForceScriptCompilation(string defines = null)
        {
            var     buildTarget         = EditorUserBuildSettings.activeBuildTarget;
            var     buildTargetGroup    = BuildPipeline.GetBuildTargetGroup(buildTarget);

            // update define symbols
            if (defines != null)
            {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, defines);
            }

            // force recompile project
#if UNITY_2019_3_OR_NEWER
            CompilationPipeline.RequestScriptCompilation();
#elif UNITY_2017_1_OR_NEWER
            var editorAssembly = System.Reflection.Assembly.GetAssembly(typeof(UnityEditor.Editor));
            var editorCompilationInterfaceType = editorAssembly.GetType("UnityEditor.Scripting.ScriptCompilation.EditorCompilationInterface");
            var dirtyAllScriptsMethod = editorCompilationInterfaceType.GetMethod("DirtyAllScripts", BindingFlags.Static | BindingFlags.Public);
            dirtyAllScriptsMethod.Invoke(editorCompilationInterfaceType, null);
#endif
        }

        public static void Build(string outputPath, params AssembliesType[] assemblyTypes)
        {



            /*
            foreach (var assemblyType in assemblyTypes)
            {
                var     assemblies              = CompilationPipeline.GetAssemblies(assemblyType);
                var     targetAssemblies        = Array.FindAll(assemblies, (item) => item.name.StartsWith("BountyRush"));
                var     sortedTargetAssemblies  = new SortedAssemblyCollection(targetAssemblies).Sort();

                var     targetOutputFolder      = $"{outputPath}/{assemblyType}Libs";
                IOServices.CreateDirectory(targetOutputFolder, overwrite: true);

                Debug.Log($"Building script libraries for {assemblyType}");
                foreach (var target in sortedTargetAssemblies)
                {
                    var     assemblyOutputPath  = $"{targetOutputFolder}/{target.name}.dll";
                    //// check whether this assembly needs to rebuilt
                    //if (!Array.Exists(targetAssemblies, (item) => string.Equals(item.name, target.name)))
                    //{
                    //    // copy file
                    //    IOServices.CopyFile(target.outputPath, assemblyOutputPath);
                    //    continue;
                    //}

                    // create reference list
                    // add direct references
                    var     referenceMap        = new Dictionary<string, string>();
                    foreach (var reference in target.assemblyReferences)
                    {
                        var     libName         = $"{reference.name}.dll";
                        referenceMap[libName]   = $"{targetOutputFolder}/{libName}";
                    }
                    // add unity libs
                    string  unityDataFolder                     = string.Format("{0}/Contents/Managed", Path.GetDirectoryName(EditorApplication.applicationContentsPath));
                    string  unityExtensionsFolder               = string.Format("{0}/Contents/UnityExtensions/Unity", Path.GetDirectoryName(EditorApplication.applicationContentsPath));
                    referenceMap["UnityEngine.dll"]             = $"{unityDataFolder}/UnityEngine.dll";
                    referenceMap["UnityEditor.dll"]             = $"{unityDataFolder}/UnityEditor.dll";
                    referenceMap["UnityEngine.UI.dll"]          = $"{unityExtensionsFolder}/GUISystem/UnityEngine.UI.dll";
                    referenceMap["UnityEngine.Networking.dll"]  = $"{unityExtensionsFolder}/Networking/UnityEngine.Networking.dll";
                    // add remaining libs suggested by unity
                    foreach (var reference in target.allReferences)
                    {
                        var     libName         = Path.GetFileName(reference);
                        if (referenceMap.ContainsKey(libName) || !reference.StartsWith("/Applications") || (libName.StartsWith("UnityEngine.") && libName.EndsWith("Module.dll")))
                        {
                            continue;
                        }
                        referenceMap[libName]   = reference;
                    }

                    // create new assembly
                    var     builder             = new AssemblyBuilderInternal(target.sourceFiles, assemblyOutputPath, EditorUserBuildSettings.activeBuildTarget);
                    builder.Defines             = target.defines;
                    builder.DefaultReferences   = referenceMap.Values.ToArray();
                    builder.CompilerOptions     = new ScriptCompilerOptions()
                    {
                        Optimize    = true,
                        FrameworkVersion     = "v4.7.1",
                        AdditionalArguments = new string[] { "-langversion:7.1" },
                    };
                    if (!builder.Build())
                    {
                        break;
                    }
                }
            }
            */
        }


        public static void BuildAssembly(string targetAssemblies, string outputPath)
        {
            BuildAssemblies(EditorUserBuildSettings.activeBuildTarget,  targetAssemblies.Split(';'), outputPath);
        }

        public static void BuildAssemblies(BuildTarget buildTarget, string[] targetAssemblies, string outputPath)
        {
            // prepare for operation
            var     buildTargetGroup        = BuildPipeline.GetBuildTargetGroup(buildTarget);
            var     editorOutputPath        = Path.Combine(outputPath, "EditorLibs");
            IOServices.CreateDirectory(editorOutputPath, overwrite: true);
            var     playerOutputPath        = Path.Combine(outputPath, "RuntimeLibs");
            IOServices.CreateDirectory(playerOutputPath, overwrite: true);

            bool    copyAllAssemblies       = (targetAssemblies == null) || (targetAssemblies.Length == 0);

            // copy player assemblies
            var     compilationSettings     = new ScriptCompilationSettings()
            {
                target  = buildTarget,
                group   = buildTargetGroup,
                options = ScriptCompilationOptions.None
            };
            var     compilationResults      = PlayerBuildInterface.CompilePlayerScripts(compilationSettings, playerOutputPath);
            if (!copyAllAssemblies)
            {
                var     playerAssemblyFiles = Directory.GetFiles(playerOutputPath);
                foreach (var path in playerAssemblyFiles)
                {
                    string  fileName        = IOServices.GetFileName(path);
                    if (!Array.Exists(targetAssemblies, (item) => fileName.Contains(item)))
                    {
                        IOServices.DeleteFile(path);
                    }
                }
            }

            // copy editor assemblies
            var     editorAssemblyies       = CompilationPipeline.GetAssemblies();
            foreach (var assembly in editorAssemblyies)
            {
                var     assemblyFilePath    = assembly.outputPath;
                if (copyAllAssemblies || Array.Exists(targetAssemblies, (item) => assemblyFilePath.Contains(item)))
                {
                    var     fileName        = Path.GetFileName(assemblyFilePath);
                    File.Copy(assemblyFilePath, Path.Combine(editorOutputPath, fileName));
                }
            }
        }

        #endregion

        #region Nested types

        private class SortedAssemblyCollection
        {
            #region Fields

            private     UnityEditor.Compilation.Assembly[]                      m_assemblies;

            private     Dictionary<string, bool>                                m_visitedAssemblies;

            private     List<UnityEditor.Compilation.Assembly>                  m_sortedAssemblies;

            #endregion

            #region Constructors

            public SortedAssemblyCollection(UnityEditor.Compilation.Assembly[] assemblies)
            {
                // set properties
                m_assemblies        = assemblies;
                m_visitedAssemblies = new Dictionary<string, bool>();
                m_sortedAssemblies  = new List<UnityEditor.Compilation.Assembly>();
            }

            #endregion

            #region Public methods

            public UnityEditor.Compilation.Assembly[] Sort()
            {
                // start iterating through objects
                foreach (var item in m_assemblies)
                {
                    if (!IsVisited(item.name))
                    {
                        DependencySortHandler(item);
                    }
                }

                return m_sortedAssemblies.ToArray();
            }

            private void DependencySortHandler(UnityEditor.Compilation.Assembly assembly)
            {
                m_visitedAssemblies[assembly.name]  = true;

                foreach (var reference in assembly.assemblyReferences)
                {
                    if (!IsVisited(reference.name))
                    {
                        DependencySortHandler(reference);
                    }
                }
                m_sortedAssemblies.Add(assembly);
            }

            private bool IsVisited(string assembly)
            {
                return m_visitedAssemblies.TryGetValue(assembly, out bool visited) && visited;
            }

            #endregion
        }

        #endregion
    }
}