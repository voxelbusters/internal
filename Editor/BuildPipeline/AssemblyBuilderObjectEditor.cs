﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using VoxelBusters.CoreLibrary.Editor;

namespace VoxelBusters.Internal.Editor
{
    [CustomEditor(typeof(AssemblyBuilderObject))]
    public class AssemblyBuilderObjectEditor : UnityEditor.Editor 
    {
        #region Fields

        private     AssemblyBuilderObject       m_exporter;
            
        private     SerializedProperty          m_assembliesProperty;

        private     SerializedProperty          m_externalLibrariesProperty;

        private     SerializedProperty          m_compilerOptionsProperty;

        private     SerializedProperty          m_outputPathProperty;

        private     SerializedProperty          m_buildTargetProperty;

        #endregion

        #region Unity methods

        private void OnEnable()
        {
            // initialise
            m_exporter                  = (AssemblyBuilderObject)target;

            m_assembliesProperty        = serializedObject.FindProperty("m_assemblies");
            m_externalLibrariesProperty = serializedObject.FindProperty("m_externalLibraries");
            m_compilerOptionsProperty   = serializedObject.FindProperty("m_compilerOptions");
            m_outputPathProperty        = serializedObject.FindProperty("m_outputPath");
            m_buildTargetProperty       = serializedObject.FindProperty("m_buildTarget");

            UpdateStaticPropertyValues();
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(m_assembliesProperty, true);
            EditorGUILayout.PropertyField(m_externalLibrariesProperty, true);
            EditorGUILayout.PropertyField(m_compilerOptionsProperty, true);
            EditorGUILayout.PropertyField(m_outputPathProperty);
            EditorGUILayout.PropertyField(m_buildTargetProperty);
            
            if (EditorGUI.EndChangeCheck() || UnityEditorUtility.GetIsEditorDirty())
            {
                UpdateStaticPropertyValues();
                serializedObject.ApplyModifiedProperties();
                UnityEditorUtility.SetIsEditorDirty(false);
            }

            if (GUILayout.Button("Build"))
            {
                m_exporter.Build((success, error) =>
                {
                    EditorUtility.RevealInFinder(m_exporter.OutputPath);
                });
            }
            if (GUILayout.Button("Show In Finder"))
            {
                EditorUtility.RevealInFinder(m_exporter.OutputPath);
            }
        }

        private void UpdateStaticPropertyValues()
        {
            AssemblyNameAttribute.SetOptions(options: m_exporter.GetAssemblyNames());
            ExternalLibraryNameAttribute.SetOptions(options: m_exporter.GetExternalLibraryNames());
            SystemLibraryNameAttribute.SetOptions(options: SystemLibraryHelpers.GetOptionalLibraryNames());
        }

        #endregion
    }
}