﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    [Serializable]
    public class SystemLibraryHelpers
    {
        #region Static fields

        private     static  readonly    LibraryDefinition                       s_systemLibrary;

        private     static  readonly    LibraryDefinition                       s_systemCoreLibrary;        

        private     static  readonly    LibraryDefinition                       s_unityEngineLibrary;

        private     static  readonly    LibraryDefinition                       s_unityEditorLibrary;

        private     static  readonly    Dictionary<string, LibraryDefinition>   s_optionalLibraries;

        #endregion

        #region Constructors

        static SystemLibraryHelpers()
        {
            string  unityDataFolder         = string.Format("{0}/Contents/Managed", Path.GetDirectoryName(EditorApplication.applicationContentsPath));
            string  unityExtensionsFolder   = string.Format("{0}/Contents/UnityExtensions/Unity", Path.GetDirectoryName(EditorApplication.applicationContentsPath));

            // set properties
            s_systemLibrary                 = new LibraryDefinition("System.dll", "System.dll");
            s_systemCoreLibrary             = new LibraryDefinition("System.Core.dll", "System.Core.dll");
            s_unityEngineLibrary            = new LibraryDefinition("UnityEngine.dll", $"{unityDataFolder}/UnityEngine.dll");
            s_unityEditorLibrary            = new LibraryDefinition("UnityEditor.dll", $"{unityDataFolder}/UnityEditor.dll");
            s_optionalLibraries             = new Dictionary<string, LibraryDefinition>()
            {
                // add system libs
                { "System.Xml.dll", new LibraryDefinition("System.Xml.dll", "System.Xml.dll") },
                { "System.Text.dll", new LibraryDefinition("System.Text.dll", "System.Text.dll") },
                { "System.Net.Http.dll", new LibraryDefinition("System.Net.Http.dll", "System.Net.Http.dll") },

                // add unity libs
                { "UnityEngine.UI.dll", new LibraryDefinition("UnityEngine.UI.dll", $"{unityExtensionsFolder}/GUISystem/UnityEngine.UI.dll") },
                { "UnityEditor.UI.dll", new LibraryDefinition("UnityEditor.UI.dll", $"{unityExtensionsFolder}/GUISystem/Editor/UnityEditor.UI.dll") },
                { "UnityEngine.Networking.dll", new LibraryDefinition("UnityEngine.Networking.dll", $"{unityExtensionsFolder}/Networking/UnityEngine.Networking.dll") },
            };
        }

        #endregion

        #region Static methods

        public static LibraryDefinition[] GetDefaultPlayerLibraries()
        {
            return new LibraryDefinition[]
            {
                s_systemLibrary,
                s_systemCoreLibrary,
                s_unityEngineLibrary
            };
        }

        public static LibraryDefinition[] GetDefaultEditorLibraries()
        {
            return new LibraryDefinition[]
            {
                s_unityEditorLibrary,
            };
        }

        public static string[] GetOptionalLibraryNames()
        {
            return s_optionalLibraries.Keys.ToArray();
        }

        public static LibraryDefinition[] GetOptionalLibraries()
        {
            return s_optionalLibraries.Values.ToArray();
        }

        public static LibraryDefinition GetSystemLibrary(string name)
        {
            s_optionalLibraries.TryGetValue(name, out LibraryDefinition reference);
            return reference;
        }

        #endregion
    }
}