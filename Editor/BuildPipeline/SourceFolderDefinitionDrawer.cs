﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VoxelBusters.Internal.Editor
{
    [CustomPropertyDrawer(typeof(SourceFolderDefinition))]
    public class SourceFolderDefinitionDrawer : PropertyDrawer 
    {
        #region Unity methods
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) 
        {
            return EditorGUIUtility.singleLineHeight;
        }
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) 
        {
            // show property name label
            EditorGUI.BeginProperty(position, label, property);
            position    = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // don't indent child properties
            var     indent          = EditorGUI.indentLevel;
            EditorGUI.indentLevel   = 0;

            var     pathRect        = new Rect(position.x, position.y, position.width - 20f, position.height);
            var     recursiveRect   = new Rect(pathRect.xMax + 5f, position.y, 15f, position.height);

            EditorGUI.PropertyField(pathRect, property.FindPropertyRelative("m_path"), GUIContent.none);
            EditorGUI.PropertyField(recursiveRect, property.FindPropertyRelative("m_recursive"), GUIContent.none);

            // reset indentation
            EditorGUI.indentLevel   = indent;

            EditorGUI.EndProperty();
        }

        #endregion
    }
}