﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    public class CsprojReference
    {
        #region Properties

        public string Name { get; private set; }

        public string Guid { get; private set; }

        public string Path { get; private set; }

        #endregion

        #region Constructors

        public CsprojReference(string name, string guid, string path)
        {
            // set properties
            Name    = name;
            Guid    = guid;
            Path    = path;
        }

        #endregion
    }
}