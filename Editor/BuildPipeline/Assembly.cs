﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    public class Assembly
    {
        #region Properties

        public string Name { get; private set; }

        public string Guid { get; private set; }

        public string Namespace { get; private set; } 

        public LibraryDefinition[] LibraryReferences { get; private set; }

        public Assembly[] AdditionalReferences { get; private set; }

        public string[] Defines { get; private set; }

        public string[] SourceFiles { get; private set; }

        public string OutputPath { get; set; }

        #endregion

        #region Constructors

        public Assembly(string name, string guid,
            string ns, LibraryDefinition[] libraryReferences,
            Assembly[] additionalReferences, string[] defines,
            string[] sourceFiles)
        {
            // set properties
            Name                    = name;
            Guid                    = guid;
            Namespace               = ns;
            LibraryReferences       = libraryReferences;
            AdditionalReferences    = additionalReferences;
            Defines                 = defines;
            SourceFiles             = sourceFiles;
        }

        #endregion
    }
}