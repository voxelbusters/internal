﻿using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor
{
    [CreateAssetMenu(fileName = "AssemblyBuilderObject", menuName = "Assembly Builder Object", order = 0)]
    public class AssemblyBuilderObject : ScriptableObject 
    {
        #region Fields

        [SerializeField, HideInInspector]
        private     string                      m_instanceGuid          = string.Empty;

        [SerializeField]
        private     List<AssemblyDefinition>    m_assemblies            = new List<AssemblyDefinition>();

        [SerializeField]
        private     List<LibraryDefinition>     m_externalLibraries     = new List<LibraryDefinition>();

        [SerializeField]
        private     ScriptCompilerOptions       m_compilerOptions       = new ScriptCompilerOptions();

        [SerializeField]
        private     string                      m_outputPath            = string.Empty;

        [SerializeField]
        private     BuildTarget                 m_buildTarget;

        #endregion

        #region Properties

        public List<AssemblyDefinition> Assemblies => m_assemblies;

        public string OutputPath => m_outputPath;

        #endregion

        #region Public methods

        public string[] GetAssemblyNames()
        {
            return m_assemblies.ConvertAll((item) => item.Name).ToArray();
        }
        
        public string[] GetExternalLibraryNames()
        {
            return m_externalLibraries.ConvertAll((item) => item.Name).ToArray();
        }

        public void Build(CompletionCallback completionCallback = null)
        {
            PrepareForBuild();

            var     assemblyBuilder = CreateAssemblyBuilder();
            assemblyBuilder.Build(m_buildTarget, completionCallback);
        }

        public void BuildEditorAssemblies(EventCallback<AssemblyBuilderResult> completionCallback = null)
        {
            PrepareForBuild();

            var     assemblyBuilder = CreateAssemblyBuilder();
            assemblyBuilder.BuildEditorAssemblies(m_buildTarget, completionCallback);
        }

        public void BuildPlayerAssemblies(EventCallback<AssemblyBuilderResult> completionCallback = null)
        {
            PrepareForBuild();

            var     assemblyBuilder = CreateAssemblyBuilder();
            assemblyBuilder.BuildPlayerAssemblies(m_buildTarget, completionCallback);
        }

        #endregion

        #region Private methods

        private void PrepareForBuild()
        {
            bool    isDirty     = false;
            // update solution guid
            if (string.IsNullOrEmpty(m_instanceGuid))
            {
                DebugLogger.Log(InternalDomain.Default, "Updating guid for solution.");
                m_instanceGuid  = Guid.NewGuid().ToString();
                isDirty         = true;
            }

            // update assembly guid
            for (int iter = 0; iter < m_assemblies.Count; iter++)
            {
                var     assembly                    = m_assemblies[iter];
                int     duplicateAssemblyGuidIndex  = m_assemblies.FindIndex((item) => (item.Guid == assembly.Guid));
                if (string.IsNullOrEmpty(assembly.Guid) || (duplicateAssemblyGuidIndex != -1 && duplicateAssemblyGuidIndex < iter))
                {
                    DebugLogger.Log(InternalDomain.Default, $"Updating guid for project: {assembly.Name}.");
                    assembly.Guid   = Guid.NewGuid().ToString();
                    isDirty         = true;
                }
            }

            if (isDirty)
            {
                EditorUtility.SetDirty(this);
            }
        }

        private AssemblyBuilder CreateAssemblyBuilder()
        {
            return new AssemblyBuilder(
                guid: m_instanceGuid,
                assemblies: m_assemblies,
                externalLibraries: m_externalLibraries,
                compilerOptions: m_compilerOptions,
                outputPath: m_outputPath);
        }

        #endregion

    }
}