﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    [Flags]
    public enum BuildConfiguration
    {
        Debug   = 1 << 0,

        Release = 1 << 1,
    }
}