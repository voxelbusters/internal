﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    [System.Serializable]
    public class ScriptCompilerOptions
    {
        #region Fields

        [SerializeField]
        private     string                  m_frameworkVersion;

        [SerializeField]
        private     string                  m_languageVersion;

        [SerializeField]
        private     bool                    m_optimize;

        [SerializeField]
        private     bool                    m_unsafe;

        [SerializeField]
        private     BuildConfiguration      m_buildConfiguration;

        [SerializeField]
        private     string[]                m_additionalArguments;

        [SerializeField]
        private     bool                    m_usePlayerDefines;

        #endregion

        #region Properties

        public string FrameworkVersion
        {
            get => m_frameworkVersion;
            set => m_frameworkVersion = value;
        }

        public string LanguageVersion
        {
            get => m_languageVersion;
            set => m_languageVersion    = value;
        }

        public bool Optimize
        {
            get => m_optimize;
            set => m_optimize   = value;
        }

        public bool Unsafe
        {
            get => m_unsafe;
            set => m_unsafe     = value;
        }

        public BuildConfiguration BuildConfiguration
        {
            get => m_buildConfiguration;
            set => m_buildConfiguration     = value;
        }

        public string[] AdditionalArguments
        {
            get => m_additionalArguments;
            set => m_additionalArguments    = value;
        }

        public bool UsePlayerDefines
        {
            get => m_usePlayerDefines;
            set => m_usePlayerDefines   = value;
        }

        #endregion

        #region Constructors

        public ScriptCompilerOptions()
        {
            // set properties
            m_frameworkVersion      = "v4.0";
            m_languageVersion       = "latest";
            m_optimize              = false;
            m_unsafe                = false;
            m_buildConfiguration    = BuildConfiguration.Debug;
            m_usePlayerDefines      = false;
        }

        #endregion
    }
}