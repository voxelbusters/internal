﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor
{
    [System.Serializable]
    public class SourceFolderDefinition
    {
        #region Fields

        [SerializeField, HideInInspector]
        private     int         m_id;

        [SerializeField, FileBrowser(usesRelativePath: true)]
        private     string      m_path;

        [SerializeField]
        private     bool        m_recursive     = true;

        #endregion

        #region Properties

        public int Id => m_id;

        public string Path => System.IO.Path.GetFullPath(m_path);

        public string Name => System.IO.Path.GetFileName(m_path);

        public bool Recursive => m_recursive;

        #endregion

        #region Constructors

        public SourceFolderDefinition(int id, string path, bool recursive)
        {
            // set properties
            m_id        = id;
            m_path      = path;
            m_recursive = recursive;
        }

        #endregion
    }
}