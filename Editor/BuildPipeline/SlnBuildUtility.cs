﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    public class SlnBuildUtility
    {
        public static string FormatPath(string path)
        {
            return path.Replace('/', '\\');
        }

        public static string ConvertBuildConfigurationToString(BuildConfiguration configuration)
        {
            switch (configuration)
            {
                case BuildConfiguration.Debug:
                    return "Debug";

                case BuildConfiguration.Release:
                    return "Release";

                default:
                    return string.Empty;
            }
        }

        public static string GetOutputFolderForConfiguration(BuildConfiguration configuration)
        {
            switch (configuration)
            {
                case BuildConfiguration.Debug:
                    return "bin/Debug";

                case BuildConfiguration.Release:
                    return "bin/Release";

                default:
                    return string.Empty;
            }
        }
    }
}