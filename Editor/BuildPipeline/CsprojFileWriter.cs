﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using VoxelBusters.CoreLibrary;

using IOPath = System.IO.Path;

namespace VoxelBusters.Internal.Editor
{
    public partial class CsprojFileWriter
    {
        #region Fields

        private     List<string>                        m_libraryReferences;

        private     List<CsprojReference>               m_projectReferences;

        private     List<string>                        m_defines;

        private     List<string>                        m_sourceFiles;

        #endregion

        #region Properties

        public string Name { get; private set; }

        public string Namespace { get; private set; }
        
        public string Version { get; private set; }

        public string Guid { get; private set; }

        public string Path { get; private set; }

        public ScriptCompilerOptions CompilerOptions { get; set; }

        #endregion

        #region Constructors

        public CsprojFileWriter(string name, string ns, string version, string guid, string path)
        {
            Name                = name;
            Namespace           = ns;
            Version             = version;
            Guid                = guid;
            Path                = path;

            // initialise
            m_libraryReferences = new List<string>();
            m_projectReferences = new List<CsprojReference>();
            m_defines           = new List<string>();
            m_sourceFiles       = new List<string>();
        }

        #endregion

        #region Configure methods

        public void AddLibraryReference(string reference)
        {
            if (!m_libraryReferences.Contains(reference))
            {
                m_libraryReferences.Add(reference);
            }
        }

        public void AddProjectReference(CsprojReference reference)
        {
            if (reference == null)
            {
                DebugLogger.LogWarning(InternalDomain.Default, "Could not find project.");
                return;
            }

            if (!m_projectReferences.Contains(reference))
            {
                m_projectReferences.Add(reference);
            }
        }

        public void AddDefineConstant(string symbol)
        {
            if (!m_defines.Contains(symbol))
            {
                m_defines.Add(symbol);
            }
        }

        public void AddSourceFile(string path)
        {
            if (!m_sourceFiles.Contains(path))
            {
                m_sourceFiles.Add(path);
            }
        }

        #endregion

        #region Serialize methods

        public CsprojReference Save()
        {
            // prepare for operation
            string  absolutePath    = IOServices.GetAbsolutePath(Path);
            string  projectFolder   = IOPath.GetDirectoryName(absolutePath);
            Directory.CreateDirectory(projectFolder);

            // serialize data
            var     writerSettings  = new XmlWriterSettings
            {
                Encoding            = new System.Text.UTF8Encoding(true),
                ConformanceLevel    = ConformanceLevel.Document,
                Indent              = true
            };
            using (var xmlWriter = XmlWriter.Create(absolutePath, writerSettings))
            {
                xmlWriter.WriteStartDocument();

                // add header
                xmlWriter.WriteStartElement("Project", "http://schemas.microsoft.com/developer/msbuild/2003");
                xmlWriter.WriteAttributeString("DefaultTargets", "Build");
                xmlWriter.WriteAttributeString("ToolsVersion", "4.0");
                xmlWriter.WriteAttributeString("xmlns", "http://schemas.microsoft.com/developer/msbuild/2003");

                xmlWriter.WriteStartElement("PropertyGroup");
                xmlWriter.WriteElement("LangVersion", CompilerOptions.LanguageVersion);
                xmlWriter.WriteEndElement();

                // add project configuration
                xmlWriter.WriteStartElement("PropertyGroup");
                xmlWriter.WriteElement("Configuration", "Condition", " '$(Configuration)' == '' ", "Debug");
                xmlWriter.WriteElement("Platform", "Condition", " '$(Platform)' == '' ", "AnyCPU");
                xmlWriter.WriteElement("ProjectGuid", "{" + Guid + "}");
                xmlWriter.WriteElement("OutputType", "Library");
                xmlWriter.WriteElement("RootNamespace", Namespace);
                xmlWriter.WriteElement("AssemblyName", Name);
                xmlWriter.WriteElement("TargetFrameworkVersion", CompilerOptions.FrameworkVersion);
                xmlWriter.WriteElement("ReleaseVersion", Version);
                xmlWriter.WriteElement("FileAlignment", "512");
                xmlWriter.WriteElement("BaseDirectory", ".");
                xmlWriter.WriteEndElement();

                // add debug configuration
                var     debugDefines    = new List<string>(m_defines);
                debugDefines.Add("DEBUG");

                xmlWriter.WriteStartElement("PropertyGroup");
                xmlWriter.WriteAttributeString("Condition", GetConfigurationCondition(BuildConfiguration.Debug));
                xmlWriter.WriteElement("DebugSymbols", true);
                xmlWriter.WriteElement("DebugType", "full");
                xmlWriter.WriteElement("Optimize", CompilerOptions.Optimize);
                xmlWriter.WriteElement("OutputPath", SlnBuildUtility.GetOutputFolderForConfiguration(BuildConfiguration.Debug));
                xmlWriter.WriteElement("DefineConstants", string.Join(";", debugDefines.ToArray()));
                xmlWriter.WriteElement("ErrorReport", "prompt");
                xmlWriter.WriteElement("WarningLevel", 4);
                xmlWriter.WriteElement("NoWarn", 0169);
                xmlWriter.WriteElement("AllowUnsafeBlocks", CompilerOptions.Unsafe);
                xmlWriter.WriteEndElement();

                // add release configuration
                var    releaseDefines   = new List<string>(m_defines);
                xmlWriter.WriteStartElement("PropertyGroup");
                xmlWriter.WriteAttributeString("Condition", GetConfigurationCondition(BuildConfiguration.Release));
                xmlWriter.WriteElement("DebugType", "pdbonly");
                xmlWriter.WriteElement("Optimize", CompilerOptions.Optimize);
                xmlWriter.WriteElement("OutputPath", SlnBuildUtility.GetOutputFolderForConfiguration(BuildConfiguration.Release));
                xmlWriter.WriteElement("DefineConstants", string.Join(";", releaseDefines.ToArray()));
                xmlWriter.WriteElement("ErrorReport", "prompt");
                xmlWriter.WriteElement("WarningLevel", 4);
                xmlWriter.WriteElement("NoWarn", 0169);
                xmlWriter.WriteElement("AllowUnsafeBlocks", CompilerOptions.Unsafe);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("PropertyGroup");
                xmlWriter.WriteElement("NoConfig", true);
                //xmlWriter.WriteElement("NoStdLib", true);
                xmlWriter.WriteElement("AddAdditionalExplicitAssemblyReferences", false);
                xmlWriter.WriteElement("ImplicitlyExpandNETStandardFacades", false);
                //xmlWriter.WriteElement("ImplicitlyExpandDesignTimeFacades", false);
                xmlWriter.WriteEndElement();

                // add library references
                xmlWriter.WriteStartElement("ItemGroup");
                foreach (var reference in m_libraryReferences)
                {
                    bool    isRooted    = reference.StartsWith("/");
                    if (isRooted)
                    {
                        xmlWriter.WriteStartElement("Reference");
                        xmlWriter.WriteAttributeString("Include", System.IO.Path.GetFileNameWithoutExtension(reference));
                        xmlWriter.WriteElement("HintPath", reference);
                        xmlWriter.WriteEndElement();
                    }
                    else
                    {
                        xmlWriter.WriteElement("Reference", "Include", reference);
                    }
                }
                xmlWriter.WriteEndElement();

                // add source file references
                xmlWriter.WriteStartElement("ItemGroup");
                foreach (string filePath in m_sourceFiles)
                {
                    string  fileAbsolutePath    = IOServices.GetAbsolutePath(filePath);
                    string  relativePath        = SlnBuildUtility.FormatPath(IOServices.GetRelativePath(projectFolder, fileAbsolutePath));
                    xmlWriter.WriteStartElement("Compile");
                    xmlWriter.WriteAttributeString("Include", relativePath);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();

                // add project references
                xmlWriter.WriteStartElement("ItemGroup");
                foreach (var reference in m_projectReferences)
                {
                    string  refProjectPath  = reference.Path;
                    string  relativePath    = SlnBuildUtility.FormatPath(IOServices.GetRelativePath(projectFolder, refProjectPath));
                    xmlWriter.WriteStartElement("ProjectReference");
                    xmlWriter.WriteAttributeString("Include", relativePath);
                    xmlWriter.WriteElement("Project", string.Format("{{{0}}}", reference.Guid));
                    xmlWriter.WriteElement("Name", reference.Name);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();

                // add footer   
                xmlWriter.WriteElement("Import", "Project", "$(MSBuildToolsPath)\\Microsoft.CSharp.targets");

                // commands section
                WriteCommands(xmlWriter, BuildConfiguration.Debug);
                WriteCommands(xmlWriter, BuildConfiguration.Release);

                // end of document
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }

            return new CsprojReference(name: Name, guid: Guid, path: Path);
        }

        #endregion

        #region Private methods

        private void WriteCommands(XmlWriter xmlWriter, BuildConfiguration configuration)
        {
            /*
            // find commands specific to given configuration
            var     commands    = m_buildCommands.FindAll((item) => item.Configuration == configuration);
            if (commands.Count > 0)
            {
                // write instructions
                xmlWriter.WriteStartElement("PropertyGroup");
                xmlWriter.WriteAttributeString("Condition", GetConfigurationCondition(configuration));
                xmlWriter.WriteStartElement("CustomCommands");
                xmlWriter.WriteStartElement("CustomCommands");
                foreach (var command in commands)
                {
                    xmlWriter.WriteStartElement("Command");
                    switch (command.EventType)
                    {
                        case SlnBuildEventType.PostBuild:
                            xmlWriter.WriteElementString("type", "AfterBuild");
                            break;
                    }
                    xmlWriter.WriteElementString("command", command.Command);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
            }
            */
        }

        private string GetConfigurationCondition(BuildConfiguration configuration)
        {
            switch (configuration)
            {
                case BuildConfiguration.Debug:
                    return " '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ";

                case BuildConfiguration.Release:
                    return " '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ";

                default:
                    return string.Empty;
            }
        }

        #endregion
    }
}