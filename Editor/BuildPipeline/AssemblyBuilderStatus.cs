﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    public enum AssemblyBuilderStatus
    {
        NotStarted,

        Compiling,

        Finished
    }
}