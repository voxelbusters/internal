﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VoxelBusters.Internal
{
    public class UnityPackageGeneratorWindow : EditorWindow
    {
        #region Fields

        [SerializeField]
        private     SerializablePackageDefinition   m_package;
        
        [SerializeField]
        private     string                          m_namespace;
        
        [SerializeField]
        private     string                          m_path;

        [SerializeField]
        private     UnityPackageGeneratorOptions    m_options;

        private     SerializedObject                m_serializedObject;

        #endregion

        #region Static methods

        [MenuItem("Window/Voxel Busters/Internal/Create Package", priority = 1)]
        public static void ShowWindow()
        {
            var     window      = (UnityPackageGeneratorWindow)GetWindow(typeof(UnityPackageGeneratorWindow));
            window.Show();
        }

        private static T DrawEnumFlagField<T>(SerializedProperty property) where T : System.Enum
        {
            var     oldValue    = (T)System.Enum.ToObject(typeof(T), property.intValue);

            // If "Everything" is set, force Unity to unset the extra bits by iterating through them
            var     newValue    = (T)EditorGUILayout.EnumFlagsField(property.displayName, oldValue);
            if ((int)(object)newValue < 0)
            {
                int     bits    = 0;
                foreach (var enumValue in System.Enum.GetValues(typeof(T)))
                {
                    int     checkBit    = (int)(object)newValue & (int)enumValue;
                    if (checkBit != 0)
                    {
                        bits   |= (int)enumValue;
                    }
                }

                newValue        = (T)(object)bits;
            }
            property.intValue   = (int)(object)newValue;
            return newValue;
        }

        private static void DrawSerializedProperty(SerializedProperty property, bool skipFirstProperty = false,
            bool hasEndProperty = true)
        {
            // move pointer to first element
            var     currentProperty = property.Copy();
            var     endProperty     = default(SerializedProperty);

            // start iterating through the properties
            bool    firstTime       = true;
            while (currentProperty.NextVisible(firstTime))
            {
                if (firstTime)
                {
                    endProperty     = hasEndProperty ? property.GetEndProperty() : null;
                    firstTime       = false;
                    if (skipFirstProperty)
                    {
                        continue;
                    }
                }
                if (hasEndProperty && SerializedProperty.EqualContents(currentProperty, endProperty))
                {
                    break;
                }

                if (currentProperty.hasChildren && currentProperty.propertyType == SerializedPropertyType.Generic)
                {
                    EditorGUILayout.LabelField(currentProperty.displayName);
                    EditorGUI.indentLevel++;
                    DrawSerializedProperty(currentProperty);
                    EditorGUI.indentLevel--;
                }
                else
                {
                    EditorGUILayout.PropertyField(currentProperty);
                }
            }
        }

        private static string GetUnityVersion()
        {
            var     components  = Application.unityVersion.Split('.');
            return $"{components[0]}.{components[1]}";
        }

        #endregion

        #region Unity methods

        private void OnEnable()
        {
            // set properties
            if (m_package == null)
            {
                m_package           = new SerializablePackageDefinition(
                    company: "",
                    version: "1.0.0",
                    unity: GetUnityVersion());
            }
        }

        private void OnGUI()
        {
            if (m_serializedObject == null)
            {
                m_serializedObject  = new SerializedObject(this);
                m_serializedObject.Update();
            }
            var     packageProperty         = m_serializedObject.FindProperty("m_package");
            var     namespaceProperty       = m_serializedObject.FindProperty("m_namespace");
            var     pathProperty            = m_serializedObject.FindProperty("m_path");
            var     optionsProperty         = m_serializedObject.FindProperty("m_options");
            DrawSerializedProperty(packageProperty);
            EditorGUILayout.PropertyField(namespaceProperty);
            EditorGUILayout.PropertyField(pathProperty);
            DrawEnumFlagField<UnityPackageGeneratorOptions>(optionsProperty);
            m_serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Generate"))
            {
               OnGenerateButtonClick(); 
            }
        }

        private void OnGenerateButtonClick()
        {
            // Fill default values for unspecified properties
            if (string.IsNullOrEmpty(m_namespace))
            {
                m_namespace     = $"{m_package.Company}.{m_package.Name}".Replace(" ", "");
            }
            if (string.IsNullOrEmpty(m_path))
            {
                m_path          = $"Assets/Plugins/{m_package.Company}";
            }

            // Invoke action
            var     packageDef  = (UnityPackageDefinition)m_package;
            var     generator   = new UnityPackageGenerator(
                package: packageDef,
                options: m_options,
                company: m_package.Company,
                ns: m_namespace,
                path: $"{m_path}/{packageDef.DisplayName}");
            generator.Generate();
            Close();
        }

        #endregion

        #region Nested types

        [System.Serializable]
        private class SerializablePackageDefinition
        {
            #region Fields

            [SerializeField]
            private     string                  m_company;

            [SerializeField]
            private     string                  m_name;

            [SerializeField]
            private     string                  m_description;

            [SerializeField]
            private     string                  m_version;

            [SerializeField]
            private     string                  m_unity;

            [SerializeField]
            private     StringKeyValuePair[]    m_dependencies;

            [SerializeField]
            private     string[]                m_keywords;

            [SerializeField]
            private     SerializableUser        m_author;

            #endregion

            #region Properties

            public string Company => m_company;

            public string Name => m_name;

            public string Description => m_description;

            public string Version => m_version;

            public string Unity => m_unity;

            public StringKeyValuePair[] Dependencies => m_dependencies;

            public string[] Keywords => m_keywords;

            public SerializableUser Author => m_author;

            #endregion

            #region Constructors

            public SerializablePackageDefinition(string company, string version, string unity)
            {
                m_company   = company;
                m_version   = version;
                m_unity     = unity;
            }

            #endregion

            #region Static methods

            public static implicit operator UnityPackageDefinition(SerializablePackageDefinition definition)
            {
                var     formattedCompanyName    = definition.Company.Replace(" ", "").ToLower();
                var     formattedPackageName    = definition.Name.Replace(" ", "").ToLower();
                return new UnityPackageDefinition(
                    name: $"com.{formattedCompanyName}.{formattedPackageName}",
                    displayName: definition.m_name,
                    description: definition.m_description,
                    version: definition.m_version,
                    unity: definition.m_unity,
                    dependencies: StringKeyValuePair.ConvertKeyValuePairsToDictionary(definition.m_dependencies),
                    keywords: definition.m_keywords,
                    author: definition.m_author);
            }

            #endregion
        }

        [System.Serializable]
        private class SerializableUser
        {
            #region Fields

            [SerializeField]
            private     string      m_name;

            [SerializeField]
            private     string      m_email;

            [SerializeField]
            private     string      m_url;

            #endregion

            #region Properties

            public string Name => m_name;

            public string Email => m_email;

            public string Url => m_url;

            #endregion

            #region Static methods

            public static implicit operator User(SerializableUser definition)
            {
                return new User(
                    name: definition.m_name,
                    email: definition.m_email,
                    url: definition.m_url);
            }

            #endregion
        }

        #endregion
    }
}