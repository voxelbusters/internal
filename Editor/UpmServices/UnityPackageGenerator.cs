﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;

namespace VoxelBusters.Internal
{
    public class UnityPackageGenerator
    {
        #region Static proprites

        private static Dictionary<UnityPackageGeneratorOptions, string> FolderMap { get; set; } = new Dictionary<UnityPackageGeneratorOptions, string>()
        {
            { UnityPackageGeneratorOptions.IncludeRuntime, "Runtime" },
            { UnityPackageGeneratorOptions.IncludeRuntimeTests, "Tests/Runtime" },
            { UnityPackageGeneratorOptions.IncludeEditor, "Editor" },
            { UnityPackageGeneratorOptions.IncludeEditorTests, "Tests/Editor" },
            { UnityPackageGeneratorOptions.IncludeResources, "Resources" },
            { UnityPackageGeneratorOptions.IncludeEditorResources, "EditorResources" },
            { UnityPackageGeneratorOptions.IncludeDocumentation, "Documentation" },
        };

        private static Dictionary<string, string> RuntimeTemplateFilesMap { get; set; } = new Dictionary<string, string>()
        {
            { "EditorResources/Templates/AssetConstants.txt", "Runtime/Core/Constants/AssetConstants.cs" },
            { "EditorResources/Templates/PackageSettings.txt", "Runtime/Core/Setup/{{SettingsClassName}}.cs" },
        };

        private static Dictionary<string, string> EditorTemplateFilesMap { get; set; } = new Dictionary<string, string>()
        {
            { "EditorResources/Templates/AssemblyInfo.txt", "Runtime/Core/AssemblyInfo.cs" },
            { "EditorResources/Templates/PackageEditorUtility.txt", "Editor/Core/{{EditorUtilityClassName}}.cs" },
            { "EditorResources/Templates/PackageMenuManager.txt", "Editor/Core/{{MenuManagerClassName}}.cs" },
            { "EditorResources/Templates/PackageSettingsEditorUtility.txt", "Editor/Core/{{SettingsEditorUtilityClassName}}.cs" },
            { "EditorResources/Templates/PackageSettingsInspector.txt", "Editor/Core/Inspectors/{{SettingsInspectorClassName}}.cs" },
            { "EditorResources/Templates/UninstallPlugin.txt", "Editor/Core/UninstallPlugin.cs" },
        };

        #endregion

        #region Properties

        public UnityPackageDefinition Package { get; private set; }

        public UnityPackageGeneratorOptions Options { get; private set; }

        public string Company { get; private set; }

        public string Namespace { get; private set; }

        public string Path { get; private set; }

        #endregion

        #region Constructors

        public UnityPackageGenerator(UnityPackageDefinition package, UnityPackageGeneratorOptions options,
            string company, string ns,
            string path)
        {
            // set properties
            Package     = package;
            Options     = options;
            Company     = company;
            Namespace   = ns;
            Path        = RemoveWhiteSpaces(path);
        }

        #endregion

        #region Static methods

        private static string RemoveWhiteSpaces(string str)
        {
            return str.Replace(" ", "");
        }

        private static string ReplaceContents(string contents, KeyValuePair<string, string>[] symbolsAndValues)
        {
            var     allLines        = contents.Split('\n');
            for (int iter = 0; iter < allLines.Length; iter++)
            {
                var     currentLine = allLines[iter];
                foreach (var item in symbolsAndValues)
                {
                    currentLine     = currentLine.Replace(item.Key, item.Value);
                }
                allLines[iter]      = currentLine;
            }
            return string.Join("\n", allLines);
        }

        private static void CreateFolders(string mainFolder, string[] subFolders)
        {
            foreach (var subFolder in subFolders)
            {
                var     pathComponents  = subFolder.Split('/');
                var     currentPath     = mainFolder;
                foreach (var item in pathComponents)
                {
                    var     newPath     = $"{currentPath}/{item}";
                    if (!AssetDatabase.IsValidFolder(newPath))
                    {
                        AssetDatabase.CreateFolder(currentPath, item);
                    }
                    currentPath         = newPath;
                }
            }
        }

        private static string SerializeObjectAsTextAsset(string path, object obj)
        {
            var     jsonText        = JsonConvert.SerializeObject(obj, Formatting.Indented);
            File.WriteAllText(path, jsonText);
            AssetDatabase.Refresh();

            return AssetDatabase.AssetPathToGUID(path);
        }

        private static bool HasValue(UnityPackageGeneratorOptions options, UnityPackageGeneratorOptions value)
        {
            return (options & value) != 0;
        }

        #endregion

        #region Public methods

        public void Generate()
        {
            // Check whether specified package name exists
            if (Directory.Exists(Path))
            {
                var     canReplace  = EditorUtility.DisplayDialog("Replace Package?", "Are you sure you want to replace existing package", "Ok", "Cancel");
                if (!canReplace) return;

                Directory.Delete(Path, true);
            }

            // Create default assets
            Directory.CreateDirectory(Path);
            SerializeObjectAsTextAsset(path: $"{Path}/package.json", obj: Package);

            // Create required folders
            foreach (var item in FolderMap)
            {
                if (HasValue(options: Options, value: item.Key))
                {
                    CreateFolders(mainFolder: Path, subFolders: item.Value.Split(';'));
                }
            }

            // Create runtime files
            var     runtimeReferences   = new List<string>()
            {
                "VoxelBusters.CoreLibrary",
                "VoxelBusters.CoreLibrary.NativePlugins"
            };
            var     editorReferences    = new List<string>(runtimeReferences)
            {
                "VoxelBusters.CoreLibrary.Editor",
            };
            var     friendlyAssemblies  = new List<string>();
            if (Options.HasFlag(UnityPackageGeneratorOptions.IncludeRuntime))
            {
                editorReferences.Add(Namespace);
                friendlyAssemblies.Add(Namespace);

                CreateRuntimeFiles(references: runtimeReferences.ToArray());

                // Tests section
                if (Options.HasFlag(UnityPackageGeneratorOptions.IncludeRuntimeTests))
                {
                    string  testsName   = $"{Namespace}.Tests";
                    CreateTestsAssemblyDefinition(
                        path: $"{Path}/Tests/Runtime/{testsName}.asmdef",
                        name: testsName,
                        references: new List<string>(runtimeReferences)
                        {
                            Namespace
                        }.ToArray());
                }
            }
            // Create editor files
            if (Options.HasFlag(UnityPackageGeneratorOptions.IncludeEditor))
            {
                string  editorNamespace = $"{Namespace}.Editor";
                friendlyAssemblies.Add(editorNamespace);

                CreateEditorFiles(references: editorReferences.ToArray());

                // Tests section
                if (Options.HasFlag(UnityPackageGeneratorOptions.IncludeRuntimeTests))
                {
                    string  testsName   = $"{editorNamespace}.Tests";
                    CreateTestsAssemblyDefinition(
                        path: $"{Path}/Tests/Editor/{testsName}.asmdef",
                        name: testsName,
                        references: new List<string>(editorReferences)
                        {
                            editorNamespace
                        }.ToArray());
                }
            }
            // Perform dependent operations
            UpdateFriendlyAssemblyReferences(assemblies: friendlyAssemblies.ToArray());
        }

        #endregion

        #region Private methods

        private void CreateRuntimeFiles(string[] references)
        {
            // Create assembly definition
            var     runtimeAssemblyDef      = new AssemblyDefinition(
                name: Namespace,
                references: references,
                overrideReferences: true,
                autoReferenced: true);
            SerializeObjectAsTextAsset(
                path: $"{Path}/Runtime/{Namespace}.asmdef",
                obj: runtimeAssemblyDef);

            // Create scripts
            CreateCommonScriptFiles(templateFilesMap: RuntimeTemplateFilesMap);
        }

        private void CreateEditorFiles(string[] references)
        {
            // Create assembly definition
            var     editorAssemblyName      = $"{Namespace}.Editor";
            var     editorAssemblyDef       = new AssemblyDefinition(
                name: editorAssemblyName,
                references: references,
                includePlatforms: new string[] { "Editor" },
                overrideReferences: true,
                autoReferenced: true);
            SerializeObjectAsTextAsset(
                path: $"{Path}/Editor/{editorAssemblyName}.asmdef",
                obj: editorAssemblyDef);

            // Create scripts
            CreateCommonScriptFiles(templateFilesMap: EditorTemplateFilesMap);
        }

        private void CreateCommonScriptFiles(Dictionary<string, string> templateFilesMap)
        {
            var     internalPackagePath = "Packages/com.voxelbusters.internal";

            // Copy template files to package folder
            string  nicifiedPackageName = RemoveWhiteSpaces(Package.DisplayName);
            var     replacementSymbolsAndValues = new KeyValuePair<string, string>[]
            {
                new KeyValuePair<string, string>("{{Namespace}}", Namespace ),
                new KeyValuePair<string, string>("{{Company}}", Company ),
                new KeyValuePair<string, string>("{{PackageName}}", Package.Name ),
                new KeyValuePair<string, string>("{{PackageDisplayName}}", Package.DisplayName ),
                new KeyValuePair<string, string>("{{PackageVersion}}", Package.Version ),
                new KeyValuePair<string, string>("{{PackagePath}}", Path ),
                new KeyValuePair<string, string>("{{SettingsClassName}}", $"{nicifiedPackageName}Settings" ),
                new KeyValuePair<string, string>("{{SettingsInspectorClassName}}", $"{nicifiedPackageName}SettingsInspector" ),
                new KeyValuePair<string, string>("{{MenuManagerClassName}}", $"{nicifiedPackageName}MenuManager" ),
                new KeyValuePair<string, string>("{{EditorUtilityClassName}}", $"{nicifiedPackageName}EditorUtility" ),
                new KeyValuePair<string, string>("{{SettingsEditorUtilityClassName}}", $"{nicifiedPackageName}SettingsEditorUtility" ),
                new KeyValuePair<string, string>("{{DefineSymbol}}", $"ENABLE_{Company}_{Package.DisplayName}".ToUpper().Replace(" ", "_") ),
                new KeyValuePair<string, string>("{{AndroidProjectName}}", Package.Name ),
                new KeyValuePair<string, string>("{{AndroidManifestClassGeneratorClass}}", Namespace.ToLower()),
            };
            foreach (var entry in templateFilesMap)
            {
                var     sourceTemplate  = $"{internalPackagePath}/{entry.Key}";
                var     sourceContents  = File.ReadAllText(sourceTemplate);
                var     scriptContents  = ReplaceContents(sourceContents, replacementSymbolsAndValues);
                var     scriptPath      = $"{Path}/{ReplaceContents(entry.Value, replacementSymbolsAndValues)}";

                // Check whether this directory exists
                var     scriptFolder    = new DirectoryInfo(System.IO.Path.GetDirectoryName(scriptPath));
                if (!scriptFolder.Exists)
                {
                    scriptFolder.Create();
                }

                // Create the file
                File.WriteAllText(scriptPath, scriptContents);
            }
        }

        private void CreateTestsAssemblyDefinition(string path, string name, string[] references)
        {
            var     testAssemblyDef     = new AssemblyDefinition(
                name: name,
                references: references,
                optionalUnityReferences: new string[] { "TestAssemblies" },
                includePlatforms: new string[] { "Editor" },
                overrideReferences: true,
                precompiledReferences: new string[] { "NSubstitute.dll" },
                autoReferenced: true);
            SerializeObjectAsTextAsset(
                path: path,
                obj: testAssemblyDef);
        }

        private void UpdateFriendlyAssemblyReferences(string[] assemblies)
        {
            if (assemblies.Length == 0) return;

            // Add specified references
            var     assemblyInfoPath    = "Assets/Plugins/VoxelBusters/CoreLibrary/Runtime/CoreLibrary/AssemblyInfo.cs";
            var     contents            = new List<string>(File.ReadAllLines(assemblyInfoPath));
            int     contentsCount       = contents.Count;
            bool    addComments         = true;
            foreach (var item in assemblies)
            {
                if (!contents.Exists((line) => line.Contains(item)))
                {
                    if (addComments)
                    {
                        contents.Add(string.Empty);
                        contents.Add($"// {Package.DisplayName}");
                        addComments     = false;
                    }
                    contents.Add($"[assembly: InternalsVisibleTo(\"{item}\")]");
                }
            }
            File.WriteAllLines(assemblyInfoPath, contents);
        }

        #endregion
    }
}