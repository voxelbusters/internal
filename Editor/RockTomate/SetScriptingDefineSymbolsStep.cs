﻿using System.Linq;
using System.Collections.Generic;
using UnityEditor;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Steps;
using HardCodeLab.RockTomate.Core.Logging;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Set Scripting Define Symbols", "Set scripting define symbols.", StepCategories.BuildingCategory)]
    public class SetScriptingDefineSymbolsStep : SimpleStep
    {
        [InputField]
        public BuildTarget[] BuildTargets;

        [InputField]
        public string Defines;

        protected override bool OnValidate()
        {
            if (string.IsNullOrEmpty(Defines))
            {
                RockLog.WriteLine(this, LogTier.Error, "Defines not specified");
                return false;
            }

            return base.OnValidate();
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            // fallback case uses all targets defined by unity
            if ((BuildTargets == null) || (BuildTargets.Length == 0))
            {
                var     enumValues  = System.Enum.GetValues(typeof(BuildTarget));
                var     targetList  = new List<BuildTarget>();
                foreach (var value in enumValues)
                {
                    targetList.Add((BuildTarget)value);
                }
                BuildTargets        = targetList.ToArray();
            }

            // set defines for every specified target
            foreach (var target in BuildTargets)
            {
                var     buildTargetGroup    = BuildPipeline.GetBuildTargetGroup(target);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, Defines);
            }

            return true;
        }
    }
}