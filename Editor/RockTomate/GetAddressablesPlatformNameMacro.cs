﻿#if UNITY_ADDRESSABLES
using HardCodeLab.RockTomate.Core.Data;
using HardCodeLab.RockTomate.Core.Enums;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Macros;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [Macro(FieldType.Input, "getAddressablesPlatformName")]
    public class GetAddressablesPlatformNameMacro : BaseMacro<string>
    {
        protected override Parameter[] GetParameters()
        {
            return new Parameter[0];
        }

        protected override string OnInvoke(JobContext context, params object[] args)
        {
            return PlatformMappingService.GetPlatform().ToString();
        }
    }
}
#endif