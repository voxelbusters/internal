﻿#if UNITY_ADDRESSABLES
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AddressableAssets;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Settings;
using System.IO;
using UnityEngine.AddressableAssets.Initialization;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Build Addressables Player Content", "Build addressables player.", StepCategories.BuildingCategory)]
    public class BuildAddressablesPlayerContentStep : SimpleStep
    {
        [InputField]
        public string Profile;

        [InputField]
        public bool DevelopmentBuild = false;

        protected override bool OnValidate()
        {
            if (AddressableAssetSettingsDefaultObject.Settings == null)
            {
                return false;
            }

            return base.OnValidate();
        }  

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            // prepare for building assets
            var     settings            = AddressableAssetSettingsDefaultObject.Settings;
            var     targetProfileId     = settings.profileSettings.GetProfileId(Profile);
            if (targetProfileId != settings.activeProfileId)
            {
                settings.activeProfileId    = targetProfileId;
                EditorUtility.SetDirty(settings);
            }
            AddressablesRuntimeProperties.ClearCachedPropertyValues();

            // initiate build process
            string  contentStateDataPath    = ContentUpdateScript.GetContentStateDataPath(false);
            if (!File.Exists(contentStateDataPath))
            {
                Debug.LogWarning("Previous Content State Data missing");
                AddressableAssetSettings.CleanPlayerContent();

                var     buildInput      = new AddressablesDataBuilderInput(settings, settings.PlayerBuildVersion);
                var     buildResult     = settings.ActivePlayerDataBuilder.BuildData<AddressablesPlayerBuildResult>(buildInput);
                return (buildResult != null) && string.IsNullOrEmpty(buildResult.Error);
            }
            else
            {
                var     buildResult         = ContentUpdateScript.BuildContentUpdate(settings, contentStateDataPath);
                return (buildResult != null) && string.IsNullOrEmpty(buildResult.Error);
            }
        }
    }
}
#endif