﻿using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Steps;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEditor;
using HardCodeLab.RockTomate.Core.Data;
using HardCodeLab.RockTomate.Core.Logging;
using VoxelBusters.CoreLibrary;
using VoxelBusters.Internal.Editor;
using VoxelBusters.Internal.Editor.DocFX;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Generate Documentation", "Generate documentation.", StepCategories.BuildingCategory)]
    public class GenerateDocumentationStep : Step
    {
        [InputField]
        public string ConfigFilePath;

        [InputField]
        public bool ClearCache = true;

        [InputField]
        public string OutputPath;

        /// <inheritdoc />
        protected override bool OnValidate()
        {
            if (string.IsNullOrEmpty(ConfigFilePath))
            {
                Debug.LogError("Invalid path.");
                return false;
            }

            return base.OnValidate();
        }

        /// <inheritdoc />
        protected override IEnumerator OnExecute(JobContext context)
        {
            var     htmlOutput  = $"{OutputPath}/Html";
            var     pdfOutput   = $"{OutputPath}/Pdf";

            // prepare for export
            IOServices.DeleteDirectory(htmlOutput);
            IOServices.DeleteDirectory(pdfOutput);

            // initiate build process
            bool    isDone      = false;
            DocumentationBuilder.Build(configFilePath: ConfigFilePath, clearCache: ClearCache, completionCallback: (result, error) =>
            {
                // mark that process is completed
                isDone      = true;

                // update task properties
                IsSuccess   = (error == null);
                if (error == null)
                {
                    // copy result data to output folder
                    if (result.HtmlBuildPath != null)
                    {
                        IOServices.CopyDirectory(result.HtmlBuildPath, htmlOutput, recursive: true);
                    }
                    if (result.PdfBuildPath != null)
                    {
                        IOServices.CopyDirectory(result.PdfBuildPath, pdfOutput, recursive: true, filePattern: "*.pdf");
                    }
                }
                else
                {
                    RockLog.WriteLine(this, LogTier.Error, $"Build failed with error : {error}.");
                }
            });

            // wait until request is completed
            while (!isDone)
            {
                yield return null;
            }
        }

        /// <inheritdoc />
        public override string InProgressText
        {
            get { return "Building"; }
        }
    }
}