﻿using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Steps;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEditor;
using HardCodeLab.RockTomate.Core.Data;
using HardCodeLab.RockTomate.Core.Logging;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Build Assemblies", "Build assemblies.", StepCategories.BuildingCategory)]
    public class BuildAssembliesStep : Step
    {
        [InputField]
        public string AssemblyBuilderAssetPath;

        [InputField(tooltip: "The platform to build")]
        public BuildTarget BuildTarget = BuildTarget.NoTarget;

        /// <inheritdoc />
        protected override IEnumerator OnExecute(JobContext context)
        {
            var     assetObject = AssetDatabase.LoadAssetAtPath<AssemblyBuilderObject>(AssemblyBuilderAssetPath);
            if (assetObject == null)
            {
                RockLog.WriteLine(this, LogTier.Error, "Could not find asset.");
                IsSuccess = false;
                yield break;
            }

            // initiate build process
            bool    isDone      = false;
            assetObject.Build((success, error) =>
            {
                // mark that process is completed
                isDone      = true;

                // update task properties
                IsSuccess   = success;
                if (error != null)
                {
                    RockLog.WriteLine(this, LogTier.Error, $"Build failed with error : {error}.");
                }
            });

            // wait until request is completed
            while (!isDone)
            {
                yield return null;
            }
        }

        /// <inheritdoc />
        public override string InProgressText
        {
            get { return "Compiling"; }
        }
    }
}