﻿using System.IO;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Steps;
using UnityEditor;
using UnityEngine;
using VoxelBusters.CoreLibrary;
using HardCodeLab.RockTomate.Core.Logging;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Copy File or Directory", "Copies a file or directory.", StepCategories.FileSystemCategory)]
    public class CopyFileOrDirectoryStep : SimpleStep
    {
        [InputField(tooltip: "Path to the file which will be copied.", required: true)]
        public string SourcePath;

        [InputField(tooltip: "Destination file path.", required: true)]
        public string DestinationPath;

        [InputField]
        public bool Overwrite;

        /// <inheritdoc />
        protected override bool OnValidate()
        {
            if (!Directory.Exists(SourcePath) && !File.Exists(SourcePath))
            {
                RockLog.WriteLine(this, LogTier.Error, string.Format("Source file/folder not found at: \"{0}\"", SourcePath));
                return false;
            }

            return true;
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            // create parent folder
            var     parent  = Directory.GetParent(DestinationPath);
            if (!parent.Exists)
            {
                parent.Create();
            }

            if (Overwrite)
            {
                if (IOServices.IsDirectory(SourcePath))
                {
                    if (IOServices.DirectoryExists(DestinationPath))
                    {
                        IOServices.DeleteDirectory(DestinationPath);
                    }
                }
                else if (IOServices.FileExists(DestinationPath))
                {
                    IOServices.DeleteFile(DestinationPath);
                }
            }

            // copy specified file
            FileUtil.CopyFileOrDirectory(SourcePath, DestinationPath);

            // copy associated meta file
            var     sourceMetaFilePath  = SourcePath + ".meta";
            if (IOServices.FileExists(sourceMetaFilePath))
            {
                var     destinationMetaFilePath = DestinationPath + ".meta";
                if (IOServices.FileExists(destinationMetaFilePath))
                {
                    IOServices.DeleteFile(destinationMetaFilePath);
                }
                FileUtil.CopyFileOrDirectory(sourceMetaFilePath, destinationMetaFilePath);
            }

            return true;
        }

        /// <inheritdoc />
        protected override string Description
        {
            get
            {
                return string.Format("Copies \"{0}\" to \"{1}\"", SourcePath, DestinationPath);
            }
        }
    }
}