﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEditor;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Updates Player Settings", "Updates player settings properties.", StepCategories.BuildingCategory)]
    public class UpdatePlayerSettingsStep : SimpleStep
    {
        [InputField]
        public string IosApplicationIdentifier;

        [InputField]
        public string AndroidApplicationIdentifier;

        [InputField]
        public string StandaloneApplicationIdentifier;

        [InputField]
        public string WebglApplicationIdentifier;

        [InputField]
        public string BundleVersion;

        [InputField]
        public string BuildNumber;

        [InputField]
        public string CompanyName;

        [InputField]
        public string ProductName;

        [InputField]
        public bool AutorotateToPortrait;

        [InputField]
        public bool AutorotateToPortraitUpsideDown;

        [InputField]
        public bool AutorotateToLandscapeRight;

        [InputField]
        public bool AutorotateToLandscapeLeft;

        private static void SetApplicationIdentifier(BuildTargetGroup targetGroup, string applicationIdentifier)
        {
            if (!string.IsNullOrEmpty(applicationIdentifier))
            {
                PlayerSettings.SetApplicationIdentifier(targetGroup, applicationIdentifier);   
            }
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            if (BuildPipeline.IsBuildTargetSupported(BuildTargetGroup.iOS, BuildTarget.iOS))
            {
                SetApplicationIdentifier(BuildTargetGroup.iOS, IosApplicationIdentifier);   
                PlayerSettings.iOS.buildNumber  = BuildNumber;
            }
            if (BuildPipeline.IsBuildTargetSupported(BuildTargetGroup.Android, BuildTarget.Android))
            {
                SetApplicationIdentifier(BuildTargetGroup.Android, AndroidApplicationIdentifier);   
            }
            if (BuildPipeline.IsBuildTargetSupported(BuildTargetGroup.WebGL, BuildTarget.WebGL))
            {
                SetApplicationIdentifier(BuildTargetGroup.WebGL, WebglApplicationIdentifier);   
            }
            if (BuildPipeline.IsBuildTargetSupported(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSX) ||
                BuildPipeline.IsBuildTargetSupported(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows))
            {
                SetApplicationIdentifier(BuildTargetGroup.Standalone, StandaloneApplicationIdentifier);   
            }
            PlayerSettings.bundleVersion                            = BundleVersion;
            PlayerSettings.companyName                              = CompanyName;
            PlayerSettings.productName                              = ProductName;
            PlayerSettings.allowedAutorotateToPortrait              = AutorotateToPortrait;
            PlayerSettings.allowedAutorotateToPortraitUpsideDown    = AutorotateToPortraitUpsideDown;
            PlayerSettings.allowedAutorotateToLandscapeRight        = AutorotateToLandscapeRight;
            PlayerSettings.allowedAutorotateToLandscapeLeft         = AutorotateToLandscapeLeft;

            return true;
        }
    }
}