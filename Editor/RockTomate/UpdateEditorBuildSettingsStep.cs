﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEditor;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Updates Build Settings", "Updates editor build settings properties.", StepCategories.BuildingCategory)]
    public class UpdateEditorBuildSettingsStep : SimpleStep
    {
        [InputField(tooltip: "The scenes to be included in the build. If empty, the currently open scene will be built. Paths are relative to the project folder (Assets/MyLevels/MyScene.unity)")]
        public string[] ScenePaths = new string[0];

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            // find valid Scene paths and make a list of EditorBuildSettingsScene
            var     targetScenes    = new List<EditorBuildSettingsScene>();
            foreach (var scenePath in ScenePaths)
            {
                if (!string.IsNullOrEmpty(scenePath))
                {
                    targetScenes.Add(new EditorBuildSettingsScene(scenePath, true));
                }
            }

            // set the Build Settings window Scene list
            EditorBuildSettings.scenes  = targetScenes.ToArray();

            return true;
        }
    }
}