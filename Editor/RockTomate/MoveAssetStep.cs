﻿using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Move Asset", "Moves asset to specified location.", StepCategories.AssetsCategory)]
    public class MoveAssetStep : SimpleStep
    {
        [InputField]
        public string AssetPath;

        [InputField]
        public string NewPath;

        /// <inheritdoc />
        protected override bool OnValidate()
        {
            var     validationResult    = AssetDatabase.ValidateMoveAsset(AssetPath, NewPath);
            if (!string.IsNullOrEmpty(validationResult))
            {
                Debug.LogError("Could not move asset. Reason: " + validationResult);
                return false;
            }

            return base.OnValidate();
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            AssetDatabase.MoveAsset(AssetPath, NewPath);
            return true;
        }
    }
}