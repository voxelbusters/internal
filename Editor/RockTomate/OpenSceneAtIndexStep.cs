﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using HardCodeLab.RockTomate.Core.Data;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Steps;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Open Scene At Index", "Opens a target unity scene in editor.", StepCategories.ScenesCategory)]
    public class OpenSceneAtIndexStep : SimpleStep
    {
        [InputField]
        public int SceneIndex;

        [InputField(tooltip: "Specify how scene should be opened")]
        public OpenSceneMode SceneMode = OpenSceneMode.Single;

        [InputField(tooltip: "Whether or not to set the scene active when it's opened.")]
        public bool SetActive = false;

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            var buildScenes = EditorBuildSettings.scenes;
            if (SceneIndex < buildScenes.Length)
            {
                var scene       = buildScenes[SceneIndex];
                var loadedScene = EditorSceneManager.OpenScene(scene.path, SceneMode);

                if (SetActive)
                    SceneManager.SetActiveScene(loadedScene);

                return true;
            }

            return false;
        }
    }
}