﻿#if UNITY_ADDRESSABLES
using System;
using System.IO;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Steps;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Copy Addressables Content Data", "Copy addressables content data.", StepCategories.AssetsCategory)]
    public class CopyAddressablesContentDataStep : SimpleStep
    {
        private const string kSourcePath = "Assets/AddressableAssetsData";

        [InputField(tooltip: "Backup folder path.", required: true)]
        public string BackupPath;

        [InputField(required: true)]
        public bool Restore;

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            var     sourceParentFolder          = Restore ? BackupPath : kSourcePath;
            var     destinationParentFolder     = Restore ? kSourcePath : BackupPath;

            var     enumValues                  = Enum.GetValues(typeof(AddressablesPlatform));
            foreach (var value in enumValues)
            {
                var     sourceContentPath       = $"{sourceParentFolder}/{value}";
                var     destinationContentPath  = $"{destinationParentFolder}/{value}";

                // prepare for copy
                if (Directory.Exists(destinationContentPath))
                {
                    Directory.Delete(destinationContentPath, recursive: true);
                    File.Delete(destinationContentPath + ".meta");
                }

                // copy folder and associated meta file
                if (Directory.Exists(sourceContentPath))
                {
                    var     parent  = Directory.GetParent(destinationContentPath);
                    if (!parent.Exists)
                    {
                        parent.Create();
                    }

                    FileUtil.CopyFileOrDirectory(sourceContentPath, destinationContentPath);
                    File.Copy(sourceContentPath + ".meta", destinationContentPath + ".meta");
                }
            }

            if (Restore)
            {
                AssetDatabase.Refresh();
            }

            return true;
        }

        /// <inheritdoc />
        protected override string Description
        {
            get
            {
                return Restore
                    ? string.Format("Copies \"{0}\" to \"{1}\"", BackupPath, kSourcePath)
                    : string.Format("Copies \"{0}\" to \"{1}\"", kSourcePath, BackupPath);
            }
        }
    }
}
#endif