﻿using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Refresh Asset Database (Synchronous)", "Synchronously imports any changed assets into the project", StepCategories.AssetsCategory)]
    public class RefreshAssetDatabaseSynchronousStep : SimpleStep
    {
        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            AssetDatabase.Refresh();
            return true;
        }
    }
}