﻿using HardCodeLab.RockTomate.Core.Data;
using HardCodeLab.RockTomate.Core.Enums;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Macros;
using System.IO;
using UnityEngine;
using UnityEditor;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [Macro(FieldType.Input, "and")]
    public class AndMacro : BaseMacro<bool>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<bool>("Value1"),
                Parameter.Create<bool>("Value2"),
            };
        }

        protected override bool OnInvoke(JobContext context, params object[] args)
        {
            var     value1  = GetArg<bool>(args, 0);
            var     value2  = GetArg<bool>(args, 1);
            return value1 && value2;
        }
    }

    [Macro(FieldType.Input, "or")]
    public class OrMacro : BaseMacro<bool>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<bool>("Value1"),
                Parameter.Create<bool>("Value2"),
            };
        }

        protected override bool OnInvoke(JobContext context, params object[] args)
        {
            var     value1  = GetArg<bool>(args, 0);
            var     value2  = GetArg<bool>(args, 1);
            return value1 || value2;
        }
    }

    [Macro(FieldType.Input, "isDirectory")]
    public class IsDirectoryMacro : BaseMacro<bool>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<string>("Path"),
            };
        }

        protected override bool OnInvoke(JobContext context, params object[] args)
        {
            var     path    = Path.GetFullPath(GetArg<string>(args, 0));
            return IOServices.IsDirectory(path);
        }
    }

    [Macro(FieldType.Input, "directoryExists")]
    public class DirectoryExistsMacro : BaseMacro<bool>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<string>("Path"),
            };
        }

        protected override bool OnInvoke(JobContext context, params object[] args)
        {
            var     path    = Path.GetFullPath(GetArg<string>(args, 0));
            return IOServices.DirectoryExists(path);
        }
    }

    [Macro(FieldType.Input, "getDirectoryName")]
    public class GetDirectoryNameMacro : BaseMacro<string>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<string>("Path"),
            };
        }

        protected override string OnInvoke(JobContext context, params object[] args)
        {
            var     path    = GetArg<string>(args, 0);
            return new DirectoryInfo(path).Name;
        }
    }

    [Macro(FieldType.Input, "getFileName")]
    public class GetFileNameMacro : BaseMacro<string>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<string>("Path"),
            };
        }

        protected override string OnInvoke(JobContext context, params object[] args)
        {
            var     path    = Path.GetFullPath(GetArg<string>(args, 0));
            return Path.GetFileName(path);
        }
    }

    [Macro(FieldType.Input, "readFileContents")]
    public class ReadFileContentsMacro : BaseMacro<string>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<string>("Path"),
            };
        }

        protected override string OnInvoke(JobContext context, params object[] args)
        {
            var     path    = Path.GetFullPath(GetArg<string>(args, 0));
            return IOServices.ReadFile(path);
        }
    }

    [Macro(FieldType.Input, "pathCombine")]
    public class PathCombineMacro : BaseMacro<string>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<string>("Path1"),
                Parameter.Create<string>("Path2"),
            };
        }

        protected override string OnInvoke(JobContext context, params object[] args)
        {
            var     path0           = GetArg<string>(args, 0);
            var     path1           = GetArg<string>(args, 1);
            return Path.Combine(path0, path1);
            /*
            var     pathFormula0    = Formula.Create(typeof(string), GetArg<string>(args, 0));
            var     pathFormula1    = Formula.Create(typeof(string), GetArg<string>(args, 1));
            pathFormula0.TryEvaluate(out object value0, context);
            pathFormula1.TryEvaluate(out object value1, context);
            return Path.Combine((string)value0, (string)value1); */
        }
    }

    [Macro(FieldType.Input, "getUnityMajorVersion")]
    public class GetUnityMajorVersionMacro : BaseMacro<string>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<string>("MajorVersion"),
            };
        }

        protected override string OnInvoke(JobContext context, params object[] args)
        {
            return Application.unityVersion.Split('.')[0];
        }
    }

    [Macro(FieldType.Input, "isActiveBuildTarget")]
    public class IsActiveBuildTargetMacro : BaseMacro<bool>
    {
        protected override Parameter[] GetParameters()
        {
            return new[]
            {
                Parameter.Create<BuildTarget>("BuildTarget"),
            };
        }

        protected override bool OnInvoke(JobContext context, params object[] args)
        {
            var     buildTarget = GetArg<BuildTarget>(args, 0);
            return (EditorUserBuildSettings.activeBuildTarget == buildTarget);
        }
    }
}