﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using UnityEditor;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Delete File or Directory", "Deletes a file (or directory)", StepCategories.FileSystemCategory)]
    public class DeleteFileOrDirectoryStep : SimpleStep
    {
        [InputField(tooltip: "Path of a file which will be deleted.", required: true)]
        public string Path;

        [InputField]
        public bool ThrowErrorOnFail = true;

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            if (FileUtil.DeleteFileOrDirectory(Path))
            {
                FileUtil.DeleteFileOrDirectory(Path + ".meta");
                return true;
            }

            return !ThrowErrorOnFail;
        }

        /// <inheritdoc />
        protected override string Description
        {
            get
            {
                return string.Format("Delete the file or folder at \"{0}\"", Path);
            }
        }
    }
}