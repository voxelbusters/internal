﻿using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Rename Asset", "Renames asset within project.", StepCategories.AssetsCategory)]
    public class RenameAssetStep : SimpleStep
    {
        [InputField]
        public string Path;

        [InputField]
        public string NewName;

        /// <inheritdoc />
        protected override bool OnValidate()
        {
            if (string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(Path)))
            {
                Debug.LogError($"Asset not found..");
                return false;
            }

            return base.OnValidate();
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            AssetDatabase.RenameAsset(Path, NewName);
            return true;
        }
    }
}