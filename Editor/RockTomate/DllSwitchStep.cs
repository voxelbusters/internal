﻿using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEditor;
using VoxelBusters.CoreLibrary;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("DLL Switch", "DLL switch.", StepCategories.BuildingCategory)]
    public class DllSwitchStep : SimpleStep
    {
        private const string kAssemblyExtension = ".dll";

        private const string kMetaFileExtension = ".meta";

        private static readonly string[] kAssetFileExtensions = { "*.prefab", "*.unity", "*.asset" };

        private const string kAssemblyVariantEditor = "EditorLibs";

        private const string kAssemblyVariantRuntime = "RuntimeLibs";

        private static readonly string[] kAssemblyVariants = new string[] { kAssemblyVariantEditor, kAssemblyVariantRuntime };

        private delegate string GenerateGuidReferenceKey(string guid, string fileId);

        [InputField]
        public SwitchMode Mode;

        [InputField]
        public string GeneratedAssembliesFolder;

        [InputField]
        public string AssembliesTargetFolder;

        [InputField]
        public string[] SourceFolders;

        [InputField]
        public string OutputPath;

        private string SourceBackupFolder;

        private static void PrintContents<K, V>(Dictionary<K, V> dict)
        {
            var     sb          = new StringBuilder(128);
            var     enumerator  = dict.GetEnumerator();
            while (enumerator.MoveNext())
            {
                sb.AppendLine($"{enumerator.Current.Key}:{enumerator.Current.Value}");
            }
            DebugLogger.Log(InternalDomain.Default, sb.ToString());
        }

        private static bool TryGetMonoScript(string guid, out MonoScript script)
        {
            var     scriptPath  = AssetDatabase.GUIDToAssetPath(guid);
            script              = AssetDatabase.LoadAssetAtPath<MonoScript>(scriptPath);
            return (script != null);
        }

        private static void FindSourceFileGuidsAtPath(string path, ref Dictionary<string, string> guidToClassNameMap)
        {
            var     scriptGuids     = AssetDatabase.FindAssets("t:MonoScript", new string[] { path });
            foreach (var monoScriptGuid in scriptGuids)
            {
                if (TryGetMonoScript(monoScriptGuid, out MonoScript script) && (script.GetClass() != null))
                {
                    guidToClassNameMap.Add(monoScriptGuid, script.GetClass().FullName);
                }
            }
        }

        private static void FindAssemblyScriptsAtPath(string path, ref Dictionary<string, SubAssetMeta> classNameToGuidMap)
        {
            var     assemblyAssets  = AssetDatabase.LoadAllAssetsAtPath(path);
            foreach (var subAsset in assemblyAssets)
            {
                if ((subAsset is MonoScript) && AssetDatabase.TryGetGUIDAndLocalFileIdentifier(subAsset, out string dllGuid, out long fileId))
                {
                    string  fullClassName   = ((MonoScript)subAsset).GetClass().FullName;
                    classNameToGuidMap.Add(fullClassName, new SubAssetMeta(guid: dllGuid, fileId: fileId.ToString()));
                }
            }
        }

        private static void UpdateScriptReferencesInAssets(GenerateGuidReferenceKey getClassReferenceKeyFunc, Dictionary<string, string> oldGUIDToClassNameMap, Dictionary<string, SubAssetMeta> newClassNameToGuidMap, string logFilePath = null)
        {
            var     output      = new StringBuilder("Report of replaced ids : \n");

            // list all the potential files that might need guid and fileID update
            var     fileList    = new List<string>();
            foreach (string extension in kAssetFileExtensions)
            {
                fileList.AddRange(Directory.GetFiles(Application.dataPath, extension, SearchOption.AllDirectories));
            }

            foreach (string file in fileList)
            {
                var     fileLines   = File.ReadAllLines(file);
                for (int iter = 0; iter < fileLines.Length; iter++)
                {
                    // find all instances of the string "guid: " and grab the next 32 characters as the old GUID
                    var     currentLine         = fileLines[iter];
                    if (currentLine.Contains("guid: ") && currentLine.Contains("fileID: "))
                    {
                        int     index           = currentLine.IndexOf("guid: ") + 6;
                        string  oldGuid         = currentLine.Substring(index, 32); // GUID has 32 characters.

                        int     index2          = currentLine.IndexOf("fileID: ") + 8;
                        int     index3          = currentLine.IndexOf(",", index2);
                        string  oldFileId       = currentLine.Substring(index2, index3 - index2);
                        
                        string  referenceKey    = getClassReferenceKeyFunc(oldGuid, oldFileId);
                        if (oldGUIDToClassNameMap.TryGetValue(referenceKey, out string className) && newClassNameToGuidMap.TryGetValue(className, out SubAssetMeta updatedClassMeta))
                        {
                            currentLine         = currentLine.Replace(oldGuid, updatedClassMeta.Guid);
                            output.AppendFormat($"[{file}]: Replacing class {className} guid: {oldGuid} with new GUID {updatedClassMeta.Guid}. ");
                            
                            currentLine         = currentLine.Replace(oldFileId, updatedClassMeta.FileId);
                            output.AppendFormat($"And replacing fileID {oldFileId} with {updatedClassMeta.FileId}");

                            fileLines[iter]     = currentLine;
                            output.Append("\n");
                        }
                    }
                }
                // write the lines back to the file
                File.WriteAllLines(file, fileLines);
            }
            if (!string.IsNullOrEmpty(logFilePath))
            {
                IOServices.CreateFile(logFilePath, output.ToString());
            }
        }

        private static void RemoveExistingDllFiles(string folder)
        {
            if (Directory.Exists(folder))
            {
                foreach (var filePath in Directory.GetFiles(folder))
                {
                    if (string.Equals(Path.GetExtension(filePath), kAssemblyExtension))
                    {
                        IOServices.DeleteFile(filePath);
                    }
                }
                foreach (var subfolderPath in Directory.GetDirectories(folder))
                {
                    RemoveExistingDllFiles(subfolderPath);
                }
            }
        }

        private static bool FileExists(string path, string extension, bool recursive)
        {
            foreach (var filePath in Directory.GetFiles(path))
            {
                if (string.Equals(Path.GetExtension(filePath), kAssemblyExtension))
                {
                    return true;
                }
            }

            if (recursive)
            {
                foreach (var subfolderPath in Directory.GetDirectories(path))
                {
                    if (FileExists(subfolderPath, extension, recursive))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        protected override bool OnValidate()
        {
            if (((Mode == SwitchMode.DllToSource) && !IOServices.DirectoryExists(GeneratedAssembliesFolder)) ||
                ((Mode == SwitchMode.SourceToDll) && !IOServices.DirectoryExists(SourceFolders[0])))
            {
                return false;
            }

            return base.OnValidate();
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            SourceBackupFolder          = IOServices.CombinePath(OutputPath, "SourceBackup");
            if (Mode == SwitchMode.SourceToDll)
            {
                ProcessSwitchToDll();
            }
            else if (Mode == SwitchMode.DllToSource)
            {
                ProcessSwitchToSource();
            }
            AssetDatabase.Refresh();
            return true;
        }

        private void ProcessSwitchToDll()
        {
            // prepare for export
            var     tempSourceFolders   = Array.ConvertAll(SourceFolders, (item) => $"{SourceBackupFolder}/{item}");
            var     snapshotPath        = GetSnapshotFilePath();
            IOServices.CreateDirectory(SourceBackupFolder, overwrite: true);

            // cache guids of the script files
            var     oldGuidToClassNameMap   = new Dictionary<string, string>(capacity: 64);
            foreach (var folder in SourceFolders)
            {
                FindSourceFileGuidsAtPath(folder, ref oldGuidToClassNameMap);
            }

            // replace script files with the specified assemblies
            MoveFolders(SourceFolders, tempSourceFolders, includeMeta: true);
            var     importedAssemblies  = ImportAssemblies(GeneratedAssembliesFolder, AssembliesTargetFolder);
            AssetDatabase.Refresh();
            if (IOServices.FileExists(snapshotPath))
            {
                var     fileContents    = IOServices.ReadFile(snapshotPath);
                var     savedSnapshot   = UnityEngine.JsonUtility.FromJson<Snapshot>(fileContents);
                UpdateAssemblyGuids(importedAssemblies, savedSnapshot.Assemblies);
            }

            // find all assets using replaced scripts and update script references with new guids
            var     newGuidCollection   = new Dictionary<string, SubAssetMeta>(capacity: 64);
            var     assemblyFiles       = Directory.GetFiles($"{AssembliesTargetFolder}/{kAssemblyVariantEditor}", "*.dll");
            for (int iter = 0; iter < assemblyFiles.Length; iter++)
            {
                var     assemblyFullPath        = assemblyFiles[iter];
                var     assemblyRelativePath    = PathHelpers.ConvertToAssetsPath(assemblyFullPath);
                FindAssemblyScriptsAtPath(assemblyRelativePath, ref newGuidCollection);
            }
            PrintContents(newGuidCollection);
            UpdateScriptReferencesInAssets((guid, fileId) => guid, oldGuidToClassNameMap, newGuidCollection, logFilePath: OutputPath + "/ImportDllLog.txt");

            // save export information
            var     exportedClasses                 = new List<AssemblyClassMeta>(capacity: 64);
            var     oldGuidToClassNameEnumerator    = oldGuidToClassNameMap.GetEnumerator();
            while (oldGuidToClassNameEnumerator.MoveNext())
            {
                var     oldGuid     = oldGuidToClassNameEnumerator.Current.Key;
                var     className   = oldGuidToClassNameEnumerator.Current.Value;
                if (newGuidCollection.TryGetValue(className, out SubAssetMeta assemblyClassMeta))
                {
                    exportedClasses.Add(new AssemblyClassMeta(
                        className: className,
                        oldGuid: oldGuid,
                        oldFileId: "11500000",
                        newGuid: assemblyClassMeta.Guid,
                        newFileId: assemblyClassMeta.FileId));
                }
                else if (TryGetMonoScript(oldGuid, out MonoScript script) && (script.GetClass() != null))
                {
                    Debug.Log($"Missed type: {className}", script);
                }
            }

            var     snapshot        = new Snapshot(
                assemblies: Array.ConvertAll(importedAssemblies, (item) => new AssemblyMeta(
                    internalId: GetAssemblyInternalId(item),
                    name: IOServices.GetFileName(item),
                    guid: AssetDatabase.AssetPathToGUID(item))),
                classes: exportedClasses.ToArray());
            IOServices.CreateFile(snapshotPath, UnityEngine.JsonUtility.ToJson(snapshot));
        }

        private void ProcessSwitchToSource()
        {
            var     tempSourceFolders   = Array.ConvertAll(SourceFolders, (item) => $"{SourceBackupFolder}/{item}");
            var     snapshotPath        = GetSnapshotFilePath();
            
            // read configuration
            var     fileContents        = IOServices.ReadFile(snapshotPath);
            var     snapshot            = UnityEngine.JsonUtility.FromJson<Snapshot>(fileContents);

            // update file references with original values
            var     oldGuidToClassNameMap   = new Dictionary<string, string>(capacity: 64);
            var     newGuidCollection       = new Dictionary<string, SubAssetMeta>(capacity: 64);
            for (int iter = 0; iter < snapshot.Classes.Length; iter++)
            {
                var     classMeta       = snapshot.Classes[iter];
                oldGuidToClassNameMap.Add($"{classMeta.NewGuid}_{classMeta.NewFileId}", classMeta.ClassName);
                newGuidCollection.Add(classMeta.ClassName, new SubAssetMeta(classMeta.OldGuid, classMeta.OldFileId));
            }
            UpdateScriptReferencesInAssets((guid, fileId) => $"{guid}_{fileId}", oldGuidToClassNameMap, newGuidCollection, logFilePath: OutputPath + "/ImportSourceLog.txt");

            // replace assembly files with script files
            MoveFolders(tempSourceFolders, SourceFolders, includeMeta: true);
            IOServices.DeleteDirectory(AssembliesTargetFolder);
            IOServices.DeleteFile(AssembliesTargetFolder + kMetaFileExtension);
        }

        private void UpdateAssemblyGuids(string[] importedAssemblies, AssemblyMeta[] metaArray)
        {
            for (int iter = 0; iter < importedAssemblies.Length; iter++)
            {
                var     assemblyPath    = importedAssemblies[iter];
                var     internalId      = GetAssemblyInternalId(assemblyPath);
                var     assemblyMeta    = Array.Find(metaArray, (item) => string.Equals(item.InternalId, internalId));
                if (assemblyMeta == null)
                {
                    Debug.LogWarning($"Could not find guid for assembly {assemblyPath}");
                    continue;
                }

                // update guid information in meta file
                var     metaFilePath        = assemblyPath + kMetaFileExtension;
                var     fileLines           = File.ReadAllLines(metaFilePath);
                int     guidLineIndex       = Array.FindIndex(fileLines, (item) => item.StartsWith("guid: "));
                fileLines[guidLineIndex]    = $"guid: {assemblyMeta.Guid}";
                File.WriteAllLines(metaFilePath, fileLines);
            }
            AssetDatabase.Refresh();
        }

        private void MoveFolders(string[] sourceFolders, string[] destinationFolders, bool includeMeta)
        {
            for (int iter = 0; iter < sourceFolders.Length; iter++)
            {
                var     sourceFolder        = sourceFolders[iter];
                var     destinationFolder   = destinationFolders[iter];
                IOServices.MoveDirectory(sourceFolder, destinationFolder);

                if (includeMeta)
                {
                    var     sourceFolderMeta    = sourceFolder + kMetaFileExtension;
                    IOServices.MoveFile(sourceFolderMeta, destinationFolder + kMetaFileExtension);
                }
            }
        }

        private string[] ImportAssemblies(string source, string destination)
        {
            // create target folder
            if (!IOServices.CreateDirectory("destination"))
            {
                RemoveExistingDllFiles(destination);
            }

            // copy files to the project
            var     importedAssemblies  = new List<string>(capacity: 8);
            for (int varIter = (kAssemblyVariants.Length - 1); varIter >= 0; varIter--)
            {
                var     variant                     = kAssemblyVariants[varIter];
                int     importedAssemblyStartIndex  = importedAssemblies.Count;
                var     variantSourceFolder         = IOServices.CombinePath(source, variant);
                var     variantDestinationFolder    = IOServices.CombinePath(destination, variant);
                if (!IOServices.DirectoryExists(variantSourceFolder))
                {
                    continue;
                }
                IOServices.CreateDirectory(variantDestinationFolder);

                var     assemblyFiles           = Directory.GetFiles(variantSourceFolder);
                foreach (var path in assemblyFiles)
                {
                    var     fileName            = Path.GetFileName(path);
                    if (fileName.EndsWith(kAssemblyExtension))
                    {
                        var     pathInAssets    = Path.Combine(variantDestinationFolder, fileName);
                        importedAssemblies.Add(pathInAssets);
                        IOServices.CopyFile(path, pathInAssets);
                    }
                }
                AssetDatabase.Refresh();

                // update importer settings
                AssetDatabase.StartAssetEditing();
                for (int iter = importedAssemblyStartIndex; iter < importedAssemblies.Count; iter++)
                {
                    var     item        = importedAssemblies[iter];
                    var     guid        = AssetDatabase.AssetPathToGUID(item);
                    var     plugin      = AssetImporter.GetAtPath(item) as PluginImporter;
                    if (plugin == null)
                    {
                        Debug.LogWarning($"Could not update import settings for dll: {item}");
                        continue;
                    }

                    switch (variant)
                    {
                        case kAssemblyVariantEditor:
                            plugin.SetCompatibleWithAnyPlatform(false);
                            plugin.SetCompatibleWithEditor(true);
                            break;

                        case kAssemblyVariantRuntime:
                            plugin.SetExcludeEditorFromAnyPlatform(true);
                            break;
                    }
                    plugin.SaveAndReimport();
                }
                AssetDatabase.StopAssetEditing();
            }

            return importedAssemblies.ToArray();
        }

        private string GetSnapshotFilePath()
        {
            return Path.Combine(OutputPath, "Snapshot.json");
        }

        private string GetAssemblyVariantName(string path)
        {
            return new DirectoryInfo(IOServices.GetDirectoryName(path)).Name;
        }

        private string GetAssemblyInternalId(string path)
        {
            return $"{GetAssemblyVariantName(path)}/{IOServices.GetFileName(path)}";
        }

        public enum SwitchMode
        {
            DllToSource,

            SourceToDll,
        }

        private class SubAssetMeta
        {
            public string Guid { get; private set; }

            public string FileId { get; private set; }

            public SubAssetMeta(string guid, string fileId)
            {
                Guid    = guid;
                FileId  = fileId;
            }

            public override string ToString()
            {
                return $"(AssetGuid: {Guid} FileId: {FileId})";
            }
        }

        [Serializable]
        public class AssemblyClassMeta
        {
            [SerializeField]
            private     string      m_className;

            [SerializeField]
            private     string      m_oldGuid;

            [SerializeField]
            private     string      m_oldFileId;

            [SerializeField]
            private     string      m_newGuid;

            [SerializeField]
            private     string      m_newFileId;

            public string ClassName => m_className;

            public string OldGuid => m_oldGuid;

            public string OldFileId => m_oldFileId;
            
            public string NewGuid => m_newGuid;

            public string NewFileId => m_newFileId;

            public AssemblyClassMeta(string className, string oldGuid, string oldFileId, string newGuid, string newFileId)
            {
                // set properties
                m_className     = className;
                m_oldGuid       = oldGuid;
                m_oldFileId     = oldFileId;
                m_newGuid       = newGuid;
                m_newFileId     = newFileId;
            }
        }

        [Serializable]
        public class AssemblyMeta
        {
            [SerializeField]
            private     string      m_internalId;

            [SerializeField]
            private     string      m_name;

            [SerializeField]
            private     string      m_guid;

            public string InternalId => m_internalId;

            public string Name => m_name;

            public string Guid => m_guid;

            public AssemblyMeta(string internalId, string name, string guid)
            {
                // set properties
                m_internalId    = internalId;
                m_name          = name;
                m_guid          = guid;
            }
        }

        [Serializable]
        private class Snapshot
        {
            [SerializeField]
            private     AssemblyMeta[]          m_assemblies;

            [SerializeField]
            private     AssemblyClassMeta[]     m_classes;

            public AssemblyMeta[] Assemblies => m_assemblies;

            public AssemblyClassMeta[] Classes => m_classes;

            public Snapshot(AssemblyMeta[] assemblies, AssemblyClassMeta[] classes)
            {
                // set properties
                m_assemblies    = assemblies;
                m_classes       = classes;
            }
        }
    }
}