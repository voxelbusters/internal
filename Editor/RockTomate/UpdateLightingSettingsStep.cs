﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System.Reflection;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Update Lighting Settings", "Update lighting settings properties.", StepCategories.BakingCategory)]
    public class UpdateLightingSettingsStep : SimpleStep
    {
        [InputField]
        public bool AutoGenerateGI;

        [InputField]
        public bool EnableBakedGI;

        [InputField]
        public bool EnableRealtimeGI;

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
#if !UNITY_2019
            LightingSettingsHepler.SetAutoGenerateGI(AutoGenerateGI);
            LightingSettingsHepler.SetBakedGIEnabled(EnableBakedGI);
            LightingSettingsHepler.SetRealtimeGIEnabled(EnableRealtimeGI);
#endif
            return true;
        }

#if !UNITY_2019
        public static class LightingSettingsHepler
        {
            public static void SetAutoGenerateGI(bool val)
            {
                SetInt("m_GIWorkflowMode", val ? 0 : 1);
            }

            public static void SetBakedGIEnabled(bool enabled)
            {
                SetBool("m_GISettings.m_EnableBakedLightmaps", enabled);
            }

            public static void SetRealtimeGIEnabled(bool enabled)
            {
                SetBool("m_GISettings.m_EnableRealtimeLightmaps", enabled);
            }
 
            public static void SetFloat(string name, float val)
            {
                ChangeProperty(name, property => property.floatValue= val);
            }
 
            public static void SetInt(string name, int val)
            {
                ChangeProperty(name, property => property.intValue = val);
            }
 
            public static void SetBool(string name, bool val)
            {
                ChangeProperty(name, property => property.boolValue = val);
            }
 
            public static void ChangeProperty(string name, System.Action<SerializedProperty> changer)
            {
                var lightmapSettings = GetLighmapSettings();
                var prop = lightmapSettings.FindProperty(name);
                if (prop != null)
                {
                    changer(prop);
                    lightmapSettings.ApplyModifiedProperties();
                }
                else Debug.LogError("lighmap property not found: " + name);
            }
 
            static SerializedObject GetLighmapSettings()
            {
                var getLightmapSettingsMethod = typeof(LightmapEditorSettings).GetMethod("GetLightmapSettings", BindingFlags.Static | BindingFlags.NonPublic);
                var lightmapSettings = getLightmapSettingsMethod.Invoke(null, null) as Object;
                return new SerializedObject(lightmapSettings);
            }
        }
#endif
    }
}