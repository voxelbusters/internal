﻿using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [StepDescription("Create Folder", "Creates folder within project.", StepCategories.AssetsCategory)]
    public class CreateAssetFolderStep : SimpleStep
    {
        [InputField]
        public string ParentFolder;

        [InputField]
        public string NewFolderName;

        /// <inheritdoc />
        protected override bool OnValidate()
        {
            if (!AssetDatabase.IsValidFolder(ParentFolder))
            {
                Debug.LogError($"Parent folder not found..");
                return false;
            }

            return base.OnValidate();
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            AssetDatabase.CreateFolder(ParentFolder, NewFolderName);
            return true;
        }
    }
}