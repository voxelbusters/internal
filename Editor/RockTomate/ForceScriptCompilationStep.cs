﻿using HardCodeLab.RockTomate;
using HardCodeLab.RockTomate.Core.Steps;
using HardCodeLab.RockTomate.Core.Attributes;
using HardCodeLab.RockTomate.Core.Helpers;
using HardCodeLab.RockTomate.Steps;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace VoxelBusters.Internal.Editor.RockTomateExtensions
{
    [InitializeOnLoad]
    [StepDescription("Force Script Compilation", "Force script compilation.", StepCategories.BuildingCategory)]
    public class ForceScriptCompilationStep : SimpleStep
    {
        private const string kRunJobOnAssemblyReload    = "RunJobOnAssemblyReload";

        [InputField]
        public string RunJobOnComplete;

        static ForceScriptCompilationStep()
        {
            EditorApplication.delayCall += OnAssemblyReloadComplete;
        }

        /// <inheritdoc />
        protected override bool OnStepStart()
        {
            if (!string.IsNullOrEmpty(RunJobOnComplete))
            {
                EditorPrefs.SetString(kRunJobOnAssemblyReload, RunJobOnComplete);
            }

            // initate script rebuild
            AssemblyBuildServices.ForceScriptCompilation();

            return true;
        }

        private static void OnAssemblyReloadComplete()
        {
            string  pendingJob  = EditorPrefs.GetString(kRunJobOnAssemblyReload, null);
            if (!string.IsNullOrEmpty(pendingJob))
            {
                Debug.Log("After Assembly Reload");
                EditorPrefs.DeleteKey(kRunJobOnAssemblyReload);
                Jobs.Start(pendingJob);
            }
        }
    }
}