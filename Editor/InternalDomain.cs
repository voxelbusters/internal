﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelBusters.Internal.Editor
{
    public class InternalDomain
    {
        public static string Default => "VoxelBusters.CoreLibrary.EssentialKit";
    }
}